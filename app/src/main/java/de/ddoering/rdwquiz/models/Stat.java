package de.ddoering.rdwquiz.models;

/**
 * Created by ddoering on 01.08.2015.
 */
public class Stat {
    private String name;
    private String value;

    public Stat(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
