package de.ddoering.rdwquiz.models;

import com.orm.SugarRecord;

/**
 * User Klasse für das Datenmodell
 * Liefert Variablen und Methoden für die Haltung der User in DB (SugarRecords)
 * @author cwagner
 */
public class User extends SugarRecord<User> {
    private String name;

    //Sugar dependency & konstruktor
    public User(){

    }

    /**
     * User anlegen mit Konstruktor
     * @param name Übergabe des Benutzernamens
     */
    public User(String name) {
        this.name = name;
    }

    /**
     * Zählt Zeichenanzahl des angegebenen Namens
     * @param name Übergabe des auszuwertenden Usernamens
     * @return Anzahl Zeichen
     */
    public long countUsers(String name) {
        String[] params = {name};
        return User.count(User.class, "name = ?", params);
    }

    /**
     * Gibt Stringform zurück
     * @return Name in Stringform
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Dient der Rückgabe des Benutzernamens
     * @return Benutzername
     */
    public String getName() {
        return name;
    }
}
