package de.ddoering.rdwquiz.models;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Question Klasse für das Datenmodell
 * Liefert Variablen und Methoden für die Haltung der Fragen in DB (SugarRecords)
 * @author cwagner
 */
public class Question extends SugarRecord<Question> {

    //Deklaration notwendiger Variablen
    private String title;
    private String type;
    private Cardfile cardfile;
    public static final String TYPE_NUMBER = "number";
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_TEXT_FREE = "textfree";
    public static final String TYPE_MULTI = "multi";

    /**
     * leerer Konstruktor
     */
    public Question() {
    }

    /**
     * Methode sucht alle passenden Antworten zur gestellten Frage
     * @return gibt eine Liste der Antworten zurück
     */
    public List<Answer> getAnswers() {
        return Answer.find(Answer.class, "Question = ?", getId().toString());
    }

    /**
     * Getter-Methode für den Titel der Frage
     * @return Titel der Frage
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter-Methode für den Titel der Frage
     * @param title Übergabe des zu setzenden Titels
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter-Methode für den Typ der Frage
     * @return Typ der Frage
     */
    public String getType() {
        return type;
    }

    /**
     * Setter-Methode für den Typ der Frage
     * @param type Übergabe des zu setzenden Typs für die Frage
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter-Methode für das zur Frage gehörende Cardfiles
     * @return Cardfile der Frage
     */
    public Cardfile getCardfile() {
        return cardfile;
    }

    /**
     * Setter-Methode für das zur Frage gehörende Cardfile
     * @param cardfile Übergabe des zugehörigen Cardfiles
     */
    public void setCardfile(Cardfile cardfile) {
        this.cardfile = cardfile;
    }

    /**
     * Methode sucht die vom Nutzer gegebende Antwort(en) und gibt diese als Liste zurück (nach Suche in DB)
     * @param user Übergabe des Users, der die Antwort(en) gegeben hat
     * @return Liste der gegebenen Antworten
     */
    public List<UserAnswer> getUserAnswers(String user)
    {
        String[] params = {user, getId().toString()};
        return UserAnswer.find(UserAnswer.class, "user = ? and question = ?", params);
    }

    /**
     * toString-Methode zur Rückgabe des Fragen-Titels in Stringform
     * @return Titel der Frage
     */
    @Override
    public String toString() {
        return getTitle();
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof Question)
        {
            Question question = (Question) o;
            if(question.getTitle().equals(title) && question.getCardfile().equals(cardfile))
                return true;
            else
                return false;
        }
        else {
            return false;
        }
    }
}
