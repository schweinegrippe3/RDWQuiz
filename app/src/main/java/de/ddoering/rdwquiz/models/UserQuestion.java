package de.ddoering.rdwquiz.models;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * UserQuestion Klasse für das Datenmodell
 * Dient dem Speichern des aktuellen Stand des Nutzers hinsichtlich der Fragen in versch. Klassen in der DB (SugarRecords)
 * @author cwagner
 */
public class UserQuestion extends SugarRecord<UserQuestion> {

    //Deklaration notwendiger Variablen
    private User user;
    private Question question;
    private Date lastAnswer;
    private int classification;

    //Sugar Records Dependency
    public UserQuestion () {

    }

    /**
     * Methode fügt Frage für einen Benutzer zu Beginn der ersten Klasse zu
     * @param usserId Übergabe des Users
     * @param questionId Übergabe der Frage
     */
    public UserQuestion(long usserId, long questionId) {
        user = User.findById(User.class, usserId);
        question = Question.findById(Question.class, questionId);
        classification = 1;
    }

    /**
     * Getter-Methode für die Classification (in welcher Klasse befindet sich die Frage?)
     * @return Klasse 1-7
     */
    public int getClassification() {
        return classification;
    }

    /**
     * Setter-Methode für die Classification einer Frage von 1-7
     * @param classification Gibt die Klasse an, zu der die Frage gehören soll
     * @throws IndexOutOfBoundsException Nur Klassen von 1-7 möglich
     */
    public void setClassification(int classification) throws IndexOutOfBoundsException {
        if(classification < 1 || classification > 7)
            throw new IndexOutOfBoundsException("value has to be between 1 and 7");
        this.classification = classification;
    }

    /**
     * Gibt das Datum der letztgegebenen Antwort zur Frage zurück
     * @return
     */
    public Date getLastAnswer() {
        return lastAnswer;
    }

    /**
     * Setzt das Datum, zu dem die Frage zuletzt beantwortet wurde
     * @param lastAnswer Datum der letzten Beantwortung
     */
    public void setLastAnswer(Date lastAnswer) {
        this.lastAnswer = lastAnswer;
    }

    /**
     * Gibt die Frage zurück
     * @return Frage
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Setter-Methode für die jeweilige Frage
     * @param question die zu setzende Frage
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    /**
     * Getter-Methode für den betreffenden User
     * @return betreffender User
     */
    public User getUser() {
        return user;
    }

    /**
     * Setter-Methode für den betreffenden User
     * @param user betreffender User
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Methode sorgt dafür, dass eine Frage in eine höhere Klasse rutscht --> Hochstufen bei richtiger Beantwortung
     * Ausnahme: ist bereits in Klasse 7
     */
    public void incrementClass() {
        if (classification <= 6)
            classification++;
    }

    /**
     * Methode sorgt dafür, dass eine Frage in eine niedrigere Klasse rutscht --> Runterstufen bei falscher Beantwortung
     * Ausnahme: Ist bereits in Klasse 1
     */
    public void decrementClass() {
        if(classification >= 2)
            classification--;
    }
}
