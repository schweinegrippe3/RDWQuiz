package de.ddoering.rdwquiz.models;

import com.orm.SugarRecord;

/**
 * Answer Klasse für das Datenmodell
 * Liefert Variablen und Methoden für das Datenmodell der Antworten zur Haltung in DB (SugarRecords)
 * @author cwagner
 */
public class Answer extends SugarRecord<Answer> {

    //Deklaration notwendiger Variablen
    private String title;
    private String type;
    private boolean correct;
    private Question question;

    /**
     * Getter-Methode für den Titel der Antwort
     * @return Titel der Antwort
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter-Methode für den Titel der Antwort
     * @param title der Antwort
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter-Methode für den Typ der Antwort
     * @return Typ der Antwort
     */
    public String getType() {
        return type;
    }

    /**
     * Setter-Methode für den Typ der Antwort
     * @param type zu setzender Typ für die Antwort
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Prüft, ob die Antwort korrekt war
     * @return
     */
    public boolean isCorrect() {
        return correct;
    }

    /**
     * Setzt das Boolean, ob die Antwort korrekt war
     * @param correct
     */
    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    /**
     * Liefert die zur Antwort passende Frage
     * @return
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Setzt die zur Antwort passende Frage
     * @param question
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Answer)
        {
            Answer answer = (Answer)o;
            if(title.equals(answer.getTitle()) && question.equals(answer.getQuestion()))
                return true;
            else
                return false;
        } else {
            return false;
        }
    }
}
