package de.ddoering.rdwquiz.models;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.Date;
import java.util.List;

/**
 * UserAnswer Klasse für die Haltung in der DB mittels SugarRecords
 * Klasse dient der Speicherung aller von einem User gegebenen Antworten, die in der DB gehalten werden
 * @author cwagner
 */
public class UserAnswer extends SugarRecord<UserAnswer> {

    //Deklaration notwendiger Variablen
    private Date timestamp;
    private User user;
    private Question question;
    private boolean correct;

    @Ignore
    private UserQuestion userQuestion;


    /**
     * leerer Konstruktor, Dependency für DB
     */
    public UserAnswer() {

    }

    /**
     * Konstruktor, der die Antworten eines gewissen Users zu einer bestimmten Frage zuordnet und die Korrektheit der Antwort hinterlegt
     * @param userId Parameter gibt an, von welchem User die Antwort stammt
     * @param questionId Gibt an, zu welcher Frage die Antwort abgegeben wurde
     * @param correct Gibt an, ob die abgegebene Antwort korrekt ist
     */
    public UserAnswer(long userId, long questionId, boolean correct)
    {
        User user = User.findById(User.class, userId);
        Question question = Question.findById(Question.class, questionId);

        //Zuordnung zur gewünschtem User, Frage und Antwort, Hinterlegen der Uhrzeit zur Antwortabgabe
        this.user = user;
        this.question = question;
        timestamp = new Date();
        this.correct = correct;

        String[] args = {String.valueOf(userId), String.valueOf(questionId)};
        List<UserQuestion> userQuestions = UserQuestion.find(UserQuestion.class, "user = ? and question = ?", args);
        if(userQuestions.size() < 1)
        {
            userQuestion = new UserQuestion(userId, questionId);
        } else {
            userQuestion = userQuestions.get(0);
        }

    }

    /**
     * Speichert die Antworten eines Users in der DB (save-Methode von SugarRecords)
     * Lässt Frage in Klassen hoch- oder runterstufen, je nach Ergebnis
     */
    @Override
    public void save() {
        super.save();
        if(correct) {
            userQuestion.incrementClass();
        } else {
            userQuestion.decrementClass();
        }
        userQuestion.setLastAnswer(new Date());

        userQuestion.save();
    }
}
