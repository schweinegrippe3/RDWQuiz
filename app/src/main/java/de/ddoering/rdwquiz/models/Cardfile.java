package de.ddoering.rdwquiz.models;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse hält die Fragen zu einer bestimmten Kategorie
 * @author cwagner
 */
public class Cardfile extends SugarRecord<Cardfile> {

    // Deklaration notwendiger Variablen
    private String Name;
    //privat float for the version number
    private float Version;
    //private decision parameter for the status of imported
    private boolean Imported;


    /**
     * leerer Konstruktor
     */
    public Cardfile() {
    }

    /**
     * Setzt, ob Cardfile bereits importiert wurde, oder nicht
     * @param Imported
     */
    public Cardfile(boolean Imported)
    {
        this.Imported = Imported;
    }

    /**
     * Getter-Methode für den Namen des Cardfiles
     * @return Name des Cardfiles
     */
    public String getName() {
        return Name;
    }

    /**
     * Setter-Methode für den Namen des Cardfiles
     * @param name Name des Cardfiles
     */
    public void setName(String name) {
        Name = name;
    }

    /**
     * Gibt eine Liste der zu diesem Cardfile zugehörigen Fragen zurück (Suche in DB)
     * @return Liste zugehöriger Fragen
     */
    public List<Question> getQuestions() {
        return Question.find(Question.class, "Cardfile = ?",getId().toString());
    }


    /**
     * Getter-Methode für die Version des Cardfiles
     * @return Version des Cardfiles
     */
    public float getVersion() {
        return Version;
    }

    /**
     * Setter-Methode für die Version des Cardfiles
     * @param version Übergabe der zu setzenden Version
     */
    public void setVersion(float version) {
        Version = version;
    }

    /**
     * Prüft, ob Cardfile bereits importiert wurde
     * @return
     */
    public boolean isImported() {
        return Imported;
    }

    /**
     * Setzt den Boolean-Wert, ob bereits importiert wurde, oder nicht
     * @param imported Übergabe des zu setzenden boolean (false/true)
     */
    public void setImported(boolean imported) {
        Imported = imported;
    }

    /**
     * Gibt die zur Verfügung stehende Anzahl Fragen des Cardfiles aus
     * @return Anzahl der verfügbaren Fragen
     */
    @Override
    public String toString() {
        return Name + "\n" + getQuestions().size() + " Fragen";
    }

    /**
     * Checks if the object is a instnace of cardfile and if the names are equal.
     *
     * @param o object to check if its a instance of cardfile
     * @return true or false - depending on if its a instance
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof Cardfile)
            {
            Cardfile cardfile = (Cardfile)o;
            if(cardfile.getName().equals(Name))
                return true;
            else
                return false;
        }
        else {
            return false;
        }
    }
}
