package de.ddoering.rdwquiz;

import android.os.Bundle;

/**
 * Created by ddoering on 29.07.2015.
 */
public interface FragmentInteraction {
    public void onFragmentInteraction(String what, Bundle data);
}
