package de.ddoering.rdwquiz;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.ddoering.rdwquiz.fragments.QuestConfirmFragment.QuestConfirmFragment;
import de.ddoering.rdwquiz.fragments.QuestShowFragment.QuestShowFragment;
import de.ddoering.rdwquiz.models.Answer;
import de.ddoering.rdwquiz.models.Question;
import de.ddoering.rdwquiz.models.UserAnswer;

/**
 * @author dvogeley
 * Generierung der klasse QuestActivity
 * Deklaration der Variablen
 *
 * Übernimmt das Handling der Fragen und die Navigation
 * Generierung der klasse QuestActivity
 * Deklaration der Variablen
 */
public class QuestActivity extends ActionBarActivity implements QuestConfirmFragment.OnQuestConfirmInteractionListener, QuestShowFragment.OnQuestShowInteractionListener {
    public static final String EXTRA_QUESTION_ID = "questionId";
    public static final String EXTRA_USER_ID = "userId";
    public static final int RESULT_CORRECT = 1;
    public static final int RESULT_WRONG = 2;
    public static final int RESULT_ABORTED = 3;

    public static final String TAG = "QuestActivityDebugTag";
    private long questionId;
    private long userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle data = getIntent().getExtras();
        if(data != null)
        {
            questionId = data.getLong(EXTRA_QUESTION_ID);
            userId = data.getLong(EXTRA_USER_ID);
        }
        else {
            Log.wtf(TAG, "QuestActivity erwartet eine QuestionId...");
        }

        setContentView(R.layout.activity_quest);
        init(savedInstanceState);
    }

    /**
     * Initialisierung
     * @param savedInstanceState
     */

    private void init(Bundle savedInstanceState) {
        //Result auf negativ stellen
        setResult(RESULT_ABORTED);
        if(savedInstanceState == null)
        {
            QuestShowFragment fragment = QuestShowFragment.newInstance(questionId);
            FragmentTransaction tn = getFragmentManager().beginTransaction();
            tn.add(R.id.a_quest_fragment_container, fragment);
            //tn.addToBackStack(null);
            tn.commit();
        }
    }

    /**
     * Inflate the menu; this adds items to the action bar if it is present.
     * @param menu
     * @return
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quest, menu);
        return true;
    }

    /**
     * Handle action bar item clicks here. The action bar will
     * automatically handle clicks on the Home/Up button, so long
     * as you specify a parent activity in AndroidManifest.xml.
     * @param item
     * @return item
     * Der Rüggabewert ist derjenige, der durch den Aufruf der Methode aus der
     * übergeordneten Klasse resultiert.
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.a_quest_action_exit) {
            //Result ist bereits richtig
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Prüfung der Frage ob diese richtig oder falsch ist
     */
    @Override
    public void onQuestConfirmInteraction(String what, Bundle data) {
        if(what.equals(QuestConfirmFragment.ACTION_ANSWER_CORRECT)){
            answerComplete(true);
        }
        else if(what.equals(QuestConfirmFragment.ACTION_ANSWER_WRONG)){
            answerComplete(false);
        }
    }

    /**
     * Es wird überprüft, um welchen Fragentyp es sich handelt, um die entsprechenden Abfrageoptionen auszuführen.
     *
     * @param what
     * @param data
     */

    @Override
    public void onQuestShowInteraction(String what, Bundle data) {
        if(what.equals(QuestShowFragment.ACTION_SUBMITTED))
        {
            Question question = Question.findById(Question.class, questionId);
            if(question.getType().equals(Question.TYPE_TEXT) || question.getType().equals(Question.TYPE_NUMBER))
            {
                String givenAnswer = data.getString(QuestShowFragment.EXTRA_ANSWER_TEXT);
                List<Answer> correctAnswers = Answer.find(Answer.class, "question = ?", String.valueOf(questionId));
                for(Answer answer : correctAnswers)
                {
                    if(answer.isCorrect())
                    {
                        if(answer.getTitle().equalsIgnoreCase(givenAnswer))
                        {
                            answerComplete(true);
                            return;
                        }
                    }
                }
                answerComplete(false);
                return;
            }
            else if (question.getType().equals(Question.TYPE_MULTI))
            {
                long[] answers = data.getLongArray(QuestShowFragment.EXTRA_ANSWER_IDS);
                List<Long> givenList = new ArrayList<Long>(answers.length);
                for (long n : answers)
                    givenList.add(n);

                List<Answer> correctAnswers = Answer.find(Answer.class, "question = ? and correct = ?", new String[]{String.valueOf(questionId), String.valueOf(1)});
                if(answers.length != correctAnswers.size())
                {
                    answerComplete(false);
                    return;
                }
                for (Answer answer : correctAnswers)
                {
                    if(givenList.contains(answer.getId()))
                    {
                        continue;
                    }
                    else {
                        answerComplete(false);
                        return;
                    }
                }

                answerComplete(true);

            }
            else if(question.getType().equals(Question.TYPE_TEXT_FREE))
            {
                QuestConfirmFragment fragment = QuestConfirmFragment.newInstance(data.getString(QuestShowFragment.EXTRA_ANSWER_TEXT), questionId);
                FragmentTransaction tn = getFragmentManager().beginTransaction();
                tn.replace(R.id.a_quest_fragment_container, fragment);
                tn.commit();
            }
        } else if(what.equals(QuestShowFragment.ACTION_QUEST_SEEN)){
            setResult(RESULT_WRONG);
            finish();
        }
    }

    /**
     * Dem User wird mitgeteilt, ob seine Antwort korrekt ("Richtig!") oder falsch ("Leider falsch!") war.
     * Wenn die Antowrt richtig ist, wird eine nachricht (=toast) ausgegeben
     * Funktion, die beschreibt was passiert, wenn eine frage richtig oder falsch beantwortet wurde
     * ?? was passiert noch?????????
     */

    public void answerComplete(boolean isCorrect){
        UserAnswer answer = new UserAnswer(userId, questionId, isCorrect);
        answer.save();

        if(isCorrect){
            Toast.makeText(getApplicationContext(), "Richtig!", Toast.LENGTH_SHORT).show();
            Question question = Question.findById(Question.class, questionId);
            setResult(RESULT_CORRECT);
            if(question.getType().equals(Question.TYPE_TEXT) && question.getAnswers().size() > 1){
                QuestShowFragment fragment = new QuestShowFragment().newInstance(questionId, true);
                FragmentTransaction tn = getFragmentManager().beginTransaction();
                tn.replace(R.id.a_quest_fragment_container, fragment);
                tn.commit();
            } else  {
                finish();
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "Leider falsch!", Toast.LENGTH_SHORT).show();
            QuestShowFragment fragment = new QuestShowFragment().newInstance(questionId, true);
            FragmentTransaction tn = getFragmentManager().beginTransaction();
            tn.replace(R.id.a_quest_fragment_container, fragment);
            tn.commit();
        }
    }

    /**
     * Was passiert, wenn der "zurück"-Button betätigt wird
     * nicht nur zurück, sondern auch die abfrage kommt
     * Interaktion mit dem User, ob dieser die Abfrage wirklich beenden möchte.
     * Die Methode beschreibt, was passieren soll, wenn der Back-Button gedrückt wird
     * es wird gefragt, ob die abfrage beendet werden soll
     */

    @Override
    public void onBackPressed() {
        //Quelle: http://stackoverflow.com/questions/2257963/how-to-show-a-dialog-to-confirm-that-the-user-wishes-to-exit-an-android-activity
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Abfrage beenden")
                .setMessage("Möchtest du die Abfrage wirklich beenden?")
                .setPositiveButton("Ja", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("Nein", null)
                .show();
    }
}
