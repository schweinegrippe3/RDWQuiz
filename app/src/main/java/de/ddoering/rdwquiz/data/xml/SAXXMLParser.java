package de.ddoering.rdwquiz.data.xml;

import android.os.Handler;
import android.util.Log;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import de.ddoering.rdwquiz.data.ItemFoundHandler;
import de.ddoering.rdwquiz.models.Cardfile;

/**
 * Helperklasse zum verarbeiten der questions.xml
 * @author ddoering
 */
public class SAXXMLParser {
    public static final String TAG = "SAXXMLParserDebugTag";

    /**
     * Compute questions.xml Dont run this inside the UI Thread!
     * @param inputStream Stream to questions.xml
     * @param itemFoundHandler Handlesr ACTION_* Events from SAXXMLHandler
     * @throws IOException Error while reading from stream
     * @throws SAXException Error while parsing questions.xml
     * @throws ParserConfigurationException Error in config
     */
    public static void parse(InputStream inputStream, Handler itemFoundHandler) throws IOException, SAXException, ParserConfigurationException {

        try {
            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            SAXXMLHandler saxxmlHandler = new SAXXMLHandler(itemFoundHandler);
            xmlReader.setContentHandler(saxxmlHandler);

            //BStart parsing. BLOCKING CALL!!!
            xmlReader.parse(new InputSource(inputStream));

        } catch (Exception ex)
        {
            Log.d(TAG, "Fehler beim parsen der XML in parse()");
            throw ex;
        }
    }

}
