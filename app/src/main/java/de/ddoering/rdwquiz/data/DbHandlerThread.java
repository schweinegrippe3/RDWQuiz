package de.ddoering.rdwquiz.data;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import org.xml.sax.SAXException;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.data.xml.SAXXMLParser;

/**
 * Thread to update DB with questions.xqml
 * @author ddoering
 */
public class DbHandlerThread extends Thread {
    //COntext to be used
    Context context;

    //Handler to inform host
    Handler outerHandler;

    //Handler to be used if an Item has been found
    final ItemFoundHandler itemFoundHandler;

    //Constants
    public static final String ACTION_CARDFILE_UPDATED = "cardfileUpdated";
    public static final String ACTION_THREAD_COMPLETE = "threadComplete";
    public static final String EXTRA_ACTION = "action";


    /**
     * Constructor for Thread
     * @param context Context to be used to open questions.xml
     * @param handler Handler for events
     */
    public DbHandlerThread(Context context, Handler handler) {
        this.context = context;
        this.outerHandler = handler;
        itemFoundHandler = new ItemFoundHandler();
    }

    @Override
    public void run() {
        try {
            //Beginn parsing
            SAXXMLParser.parse(context.getResources().openRawResource(R.raw.questions), itemFoundHandler);

            //Parsing completed. Inform host
            Bundle data = new Bundle();
            data.putString(EXTRA_ACTION, ACTION_THREAD_COMPLETE);
            Message message = new Message();
            message.setData(data);
            outerHandler.sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
