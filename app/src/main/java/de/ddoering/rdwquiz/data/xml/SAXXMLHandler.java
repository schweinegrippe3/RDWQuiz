package de.ddoering.rdwquiz.data.xml;

import android.os.Handler;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import de.ddoering.rdwquiz.data.ItemFoundHandler;
import de.ddoering.rdwquiz.data.db.DatabaseInserter;
import de.ddoering.rdwquiz.models.Answer;
import de.ddoering.rdwquiz.models.Cardfile;
import de.ddoering.rdwquiz.models.Question;

/**
 * Handler for questions.xml
 * @author ddoering
 *
 */
public class SAXXMLHandler extends DefaultHandler {
    public static final int ACTION_ANSWER_FOUND = 1;
    public static final int ACTION_QUESTION_FOUND = 2;
    public static final int ACTION_CARDFILE_FOUND = 3;

    private Cardfile tmpCardfile;
    private Question tmpQuestion;
    private Answer tmpAnswer;
    private DatabaseInserter databaseInserter;
    private final Handler itemFoundHandler;

    private String tmpVal;

    /**
     * Constructor. Sets used vars
     * @param handler Handler which should be used to handle found items
     */
    public SAXXMLHandler(Handler handler) {
        databaseInserter = new DatabaseInserter();
        this.itemFoundHandler = handler;
    }

    /**
     * Overide this method to fit our needs.For params, take a look at Developer ref
     * @param uri
     * @param localName
     * @param qName
     * @param attributes
     * @throws SAXException
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        tmpVal = "";
        if(qName.equalsIgnoreCase("cardfile"))
            tmpCardfile = new Cardfile(true);
        else if(qName.equalsIgnoreCase("question"))
            tmpQuestion = new Question();
        else if(qName.equalsIgnoreCase("answer"))
            tmpAnswer = new Answer();
    }

    /**
     * Overide this method to fit our needs.For params, take a look at Developer ref
     * @param ch
     * @param start
     * @param length
     * @throws SAXException
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        tmpVal = new String(ch, start, length);
    }

    /**
     * Overide this method to fit our needs.For params, take a look at Developer ref
     * @param uri
     * @param localName
     * @param qName
     * @throws SAXException
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(qName.equalsIgnoreCase("atitle"))
            tmpAnswer.setTitle(tmpVal);
        else if(qName.equalsIgnoreCase("atype"))
            tmpAnswer.setType(tmpVal);
        else if(qName.equalsIgnoreCase("correct"))
            tmpAnswer.setCorrect(Boolean.parseBoolean(tmpVal));
        else if(qName.equalsIgnoreCase("answer"))
        {
            itemFoundHandler.obtainMessage(ACTION_ANSWER_FOUND, tmpAnswer).sendToTarget();
        }
        else if(qName.equalsIgnoreCase("qtype"))
            tmpQuestion.setType(tmpVal);
        else if(qName.equalsIgnoreCase("qtitle"))
            tmpQuestion.setTitle(tmpVal);
        else if(qName.equalsIgnoreCase("question"))
        {
            itemFoundHandler.obtainMessage(ACTION_QUESTION_FOUND, tmpQuestion).sendToTarget();
        }
        else if (qName.equalsIgnoreCase("cfname"))
            tmpCardfile.setName(tmpVal);
        else if(qName.equalsIgnoreCase("cfversion"))
            tmpCardfile.setVersion(Float.parseFloat(tmpVal));
        else if(qName.equalsIgnoreCase("cardfile"))
        {
            itemFoundHandler.obtainMessage(ACTION_CARDFILE_FOUND, tmpCardfile).sendToTarget();
        }

    }
}
