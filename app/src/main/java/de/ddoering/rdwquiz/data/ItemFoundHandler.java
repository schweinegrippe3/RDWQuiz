package de.ddoering.rdwquiz.data;

import android.os.Handler;
import android.os.Message;

import de.ddoering.rdwquiz.data.db.DatabaseInserter;
import de.ddoering.rdwquiz.data.xml.SAXXMLHandler;
import de.ddoering.rdwquiz.models.Answer;
import de.ddoering.rdwquiz.models.Cardfile;
import de.ddoering.rdwquiz.models.Question;

/**
 * Handles events while Parser is parsing questions.xml
 * @author ddoering
 */
public class ItemFoundHandler extends Handler {

    final DatabaseInserter databaseInserter;

    public ItemFoundHandler() {
        databaseInserter = new DatabaseInserter();
    }

    /**
     * Override this method to fit our needs. For params and description, take a look at android dev ref
     * @param msg What happend?
     */
    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case SAXXMLHandler.ACTION_ANSWER_FOUND:
                databaseInserter.addAnswer((Answer)msg.obj);
                break;
            case SAXXMLHandler.ACTION_QUESTION_FOUND:
                databaseInserter.addQuestion((Question) msg.obj);
                break;
            case SAXXMLHandler.ACTION_CARDFILE_FOUND:
                databaseInserter.insertCardfile((Cardfile) msg.obj);
                break;
        }
    }
}
