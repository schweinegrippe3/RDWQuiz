package de.ddoering.rdwquiz.data.visualizer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.Stat;

/**
 * A class which extends ArrayAdapter to display a list with two rows
 * @author ddoering
 */
public class StatsAdapter extends ArrayAdapter<Stat> {

    private List<Stat> stats;

    public StatsAdapter(Context context, int textViewResourceId, List<Stat> stats) {
        super(context, textViewResourceId, stats);
        this.stats = stats;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.default_stats_list, null);
        }

        Stat stat = stats.get(position);
        if(stat != null)
        {
            //Set text
            TextView name = (TextView)v.findViewById(R.id.l_stats_name);
            TextView value = (TextView)v.findViewById(R.id.l_stats_value);
            if(stat.getName() != null)
                name.setText(stat.getName());
            if(stat.getValue() != null)
                value.setText(stat.getValue());
        }

        return v;
    }
}
