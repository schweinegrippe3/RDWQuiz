package de.ddoering.rdwquiz.data.db;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.ddoering.rdwquiz.models.Answer;
import de.ddoering.rdwquiz.models.Cardfile;
import de.ddoering.rdwquiz.models.Question;
import de.ddoering.rdwquiz.models.UserAnswer;
import de.ddoering.rdwquiz.models.UserQuestion;

/**
 * Handles found Elements inside the parser
 * @author ddoering
 */
public class DatabaseInserter {
    private ArrayList<Answer> tmpAnswers;
    private ArrayList<Question> tmpQuestions;

    public DatabaseInserter() {
        tmpAnswers = new ArrayList<Answer>();
        tmpQuestions = new ArrayList<Question>();
    }

    /**
     * Inserts a new Cardfile into the db.
     * If cardfile exists, itś going to do an incremental update
     * @param cardfile The cardfile which should be inserted
     */
    public void insertCardfile(Cardfile cardfile) {
        for(Question q : tmpQuestions)
            if(q.getCardfile() == null)
                q.setCardfile(cardfile);

        List<Cardfile> cardfiles = Cardfile.find(Cardfile.class, "name = ?", cardfile.getName());
        if(cardfiles.size() == 0)
        {
            //Cardfile not in db
            Cardfile.saveInTx(cardfile);
            Question.saveInTx(tmpQuestions);
            Answer.saveInTx(tmpAnswers);
        } else {
            Cardfile dbCardfile = cardfiles.get(0);
            if(cardfile.getVersion() > dbCardfile.getVersion())
            {
                //XML is newer then DB...
                updateIncremental(cardfile, tmpQuestions, tmpAnswers, dbCardfile);
            }
        }

        tmpAnswers = new ArrayList<Answer>();
        tmpQuestions = new ArrayList<Question>();
    }

    /**
     * Do incremental update
     * @param newCardfile The new cardfile
     * @param newQuestions The new questions inside the cardfile
     * @param newAnswers The new answers to the questions
     * @param oldCardfile The old cardfile
     */
    private void updateIncremental(Cardfile newCardfile, ArrayList<Question> newQuestions, ArrayList<Answer> newAnswers, Cardfile oldCardfile) {

        //List with cardfiles to be removed
        List<Question> removedQuestions = new ArrayList<>(oldCardfile.getQuestions());
        removedQuestions.removeAll(newQuestions);

        //List of new questions which are not inside db
        List<Question> addedQuestions = new ArrayList<>(newQuestions);
        addedQuestions.removeAll(oldCardfile.getQuestions());

        //Questions to be removed
        for(Question question : removedQuestions)
            removeQuestion(question);

        //List of questions to keep
        List<Question> keptQuestions = new ArrayList<>(oldCardfile.getQuestions());
        for (Question question : keptQuestions)
            updateQuestion(question, newQuestions, newAnswers);

        //Insert new questions
        for (Question question : addedQuestions)
            insertQuestion(question, newAnswers, oldCardfile);

        oldCardfile.setVersion(newCardfile.getVersion());
        oldCardfile.save();
    }

    /**
     * Remove questions and dependencys from db
     * @param question Question to be removed
     */
    private void removeQuestion(Question question) {
        //Remove dependencys
        for(Answer answer : question.getAnswers())
            answer.delete();

        for(UserAnswer userAnswer : UserAnswer.find(UserAnswer.class, "question = ?", String.valueOf(question.getId())))
            userAnswer.delete();

        for(UserQuestion userQuestion : UserQuestion.find(UserQuestion.class, "question = ?", String.valueOf(question.getId())))
            userQuestion.delete();

        //Delete questions
        question.delete();
    }

    /**
     * Insert new questions into DB
     * @param question New question
     * @param newAnswers New answers
     * @param cardfile Cardfile that should be used
     */
    private void insertQuestion(Question question, List<Answer> newAnswers, Cardfile cardfile)
    {
        //Insert dependencys
        ArrayList<Answer> answers = new ArrayList<>();
        for (Answer answer : newAnswers)
            if(answer.getQuestion().equals(question))
            {
                answer.setQuestion(question);
                answers.add(answer);
            }

        //Insert question
        question.setCardfile(cardfile);
        question.save();
        Answer.saveInTx(answers);
    }

    /**
     * Update question inside DB
     * @param oldQuestion Existing question in DB
     * @param newQuestions New question that should be used
     * @param newAnswers New Answers for the question
     */
    private void updateQuestion(Question oldQuestion, List<Question> newQuestions ,List<Answer> newAnswers)
    {
        //Map questions
        Question newQuestion = null;
        for(Question question : newQuestions)
            if(question.equals(oldQuestion))
                newQuestion = question;

        //Something bad happend :P
        if(newQuestion == null)
            return;

        //ANswers to new questions
        List<Answer> answersToNewQuestion = new ArrayList<>();
        for (Answer answer : newAnswers)
            if(answer.getQuestion().equals(newQuestion))
            {
                answer.setQuestion(oldQuestion);
                answersToNewQuestion.add(answer);
            }

        //Update Answers
        Answer.deleteAll(Answer.class, "question =  ?", String.valueOf(oldQuestion.getId()));
        Answer.saveInTx(answersToNewQuestion);

        //Update questiontype
        oldQuestion.setType(newQuestion.getType());
        oldQuestion.save();
    }

    /**
     * Handle newly found question
     * @param question The found question
     */
    public void addQuestion(Question question)
    {
        //Set dependencys
        for (Answer a : tmpAnswers)
            if(a.getQuestion() == null)
                a.setQuestion(question);
        //Set question
        tmpQuestions.add(question);
    }

    /**
     * Handle newly found Answers
     * @param answer The found answer
     */
    public void addAnswer(Answer answer)
    {
        //Set answer
        tmpAnswers.add(answer);
    }

}
