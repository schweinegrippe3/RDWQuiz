package de.ddoering.rdwquiz;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import de.ddoering.rdwquiz.fragments.LoginFragment.LoginFragment;

/**
 * Hauptactivity der App, dient v.a. zum Starten der App --> Einstiegspunkt
 * Lädt standardmäßig das Loginfragment beim Starten
 * @author cwagner
 */
public class MainActivity extends ActionBarActivity implements LoginFragment.OnLoginInteractionListener{

    //Username für den Login (wird aus dem Login Fragment und dessen Logik angefordert)
    public static final String EXTRA_USERNAME = "username";
    /**
     * Überschreibt die onCreate Methode um stattdessen das Login-Fragment zu laden
     *
     * @param savedInstanceState Bundle mit gespeicherten Informationen zum Wiederherstellen der Ansicht
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //lade Informationen aus dem Bundle und setze die View
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Wird die App komplett neu gestartet?
        if(savedInstanceState == null)
        {
            //App wurde neu gestartet, lade Login...
            LoginFragment loginFragment = new LoginFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.a_main_fragment_container, loginFragment).commit();
        }
    }

    /**
     * Überschreibt die onCreateOptionsMenu-Methode um eigene Menüeinträge verwenden zu können
     * @param menu Übergabe der gewünschten Menüelemente
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Sind Elemente für die Actionbar vorhanden, werden sie hier geladen, nachdem das Menü integriert wurde
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Methode handelt mit Klicks auf Items der Actionbar, reagiert auch auf Home und Zurückbutton
     *
     * @param item Geklicktes Item
     * @return Wurde das click Event von dieser Methode behandelt?
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    /**
     * Implementiert den Listener für das LoginFragment (da Login zum Start der App notwendig)
     * @param what Welche Aktion wurde ausgeführt (Login oder Beenden?)
     * @param data Beinhaltet zusätzliche Daten und Informationen zur Aktion
     */
    @Override
    public void onLoginInteraction(String what, Bundle data) {
        switch (what){
            case LoginFragment.ACTION_LOGIN:
                Intent intent = new Intent(this, QuizActivity.class);
                //setzt den gewählten Benutzernamen
                intent.putExtra(EXTRA_USERNAME, data.getString(EXTRA_USERNAME));
                startActivity(intent);
                break;
            case LoginFragment.ACTION_QUIT:
                //beendet die App
                finish();
                break;
        }
    }
}
