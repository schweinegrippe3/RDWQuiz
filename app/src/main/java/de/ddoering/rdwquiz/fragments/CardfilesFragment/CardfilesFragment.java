package de.ddoering.rdwquiz.fragments.CardfilesFragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import de.ddoering.rdwquiz.R;

/**
 * This fragment creates all views, interactions and logic for the cardfile menu.
 *
 * This fragment calls the ApplicationLogic, EventToListenerMapping and the UI.
 * It is called in the QuizActivity.
 *
 * @author czschoer
 */
public class CardfilesFragment extends Fragment {
    //String for the brief description by debugging
    public static final String TAG = "CardfilesFragmentTag";
    //Sting for the cardfile-ID
    public static final String EXTRA_CARDFILE_ID = "CardfileId";
    //String for the action of a click on a cardfile
    public static final String ACTION_CARDFILE_CLICKED = "simpleClick";
    //String for the action of a long click on a cardfile
    public static final String ACTION_CARDFILE_LONG_CLICKED = "longClick";

    //private String for the username argument
    private static final String ARG_USERNAME = "username";

    //private String for the username
    private String username;
    //private view for the list of the cardfiles
    private ListView cardfilesList;

    //private Ui
    private Ui ui;
    //private ApplicationLogic
    private ApplicationLogic applicationLogic;
    //private EventToListenerMapping
    private EventToListenerMapping eventToListenerMapping;

    //private OnCardfilesInteractionListener
    private OnCardfilesInteractionListener cardfilesInteraction;

    /**
     * Creates a instance of this fragment with the delivered username.
     * The username is saved into a bundle, too.
     *
     * @param username String name of the user which wish to open the view
     * @return gives a instance of this fragment back
     */
    public static CardfilesFragment newInstance(String username) {
        CardfilesFragment fragment = new CardfilesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USERNAME, username);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Creates a empty instance of this fragment
     */
    public CardfilesFragment() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of the fragment. Is called after onAttach()
     * and before onCreateView().
     * Method is override. Saves the username out of the bundle only if the arguments are not null.
     *
     * @param savedInstanceState bundle with information
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            username = getArguments().getString(ARG_USERNAME);
        }
    }

    /**
     * In this method is a view created.
     * The title is put on "Verfügbare Karteien".
     * Creates a GUI and transfer to it the view and the instance of this fragment.
     * The ApplicationLogic is created and gets transferred the GUI and the instance of this fragment.
     * As well the EventList is created with the ApplicationLogic and the GUI.
     *
     * @param inflater is used to fill the view of the fragment
     * @param container is used to generate layout parameters
     * @param savedInstanceState this fragments is returned to older status if not null
     * @return gives a view for the cardfiles back
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Verfügbare Karteien");

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cardfiles, container, false);
        init(v);

        ui = new Ui(v, this);
        applicationLogic = new ApplicationLogic(ui, this);
        eventToListenerMapping = new EventToListenerMapping(applicationLogic, ui);

        return v;
    }

    /**
     * This method is executed when the fragment is first attached to the activity.
     * Must be called before the fragment can be created.
     * It checks if the FragmentInteraction is implemented else a exception is threw.
     * Puts the fragmentInteraction on the delivered activity.
     *
     * @param activity transfer the activity to which the connection should be
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            cardfilesInteraction = (OnCardfilesInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCardfilesInteractionListener");
        }
    }

    /**
     * Initialise the view of the cardfilesList
     *
     * @param v the view of this fragment
     */
    public void init(View v){
        cardfilesList = (ListView) v.findViewById(R.id.f_cardfiles_list);
    }

    /**
     * Calls a method to update the GUI
     */
    public void update() {
        ui.updateUi();
    }

    /**
     * must be implemented to communicate with the activity and other fragments that are connected
     * to the activity.
     */
    public interface OnCardfilesInteractionListener {
        public void onCardfilesInteraction(String what, Bundle data);
    }

    /**
     *  getter for the cardfileInteraction
     *
     * @return gives the cardfileInteraction back
     */
    public OnCardfilesInteractionListener getCardfilesInteraction() {
        return cardfilesInteraction;
    }
}
