package de.ddoering.rdwquiz.fragments.QuestionsFragment;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.Question;

/**
 * Ui.java is used to edit (/present) the graphical user interface.
 * Declaration of the variables: view, questionsFragment, data, listView
 * @author smartens
 */

public class Ui {

    private final View view;
    private final QuestionsFragment questionsFragment;
    private final Data data;
    private final ListView listView;

    /** Ui()
     * Hand out the variables: view, questionsFragment, data
     * Constructor, Initialisation of the variables: view, questionsFragment, data
     * @param view
     * @param questionsFragment
     * @param data
     */

    public Ui(View view, QuestionsFragment questionsFragment, Data data) {
        this.view = view;
        this.questionsFragment = questionsFragment;
        this.data = data;
        // listView is an object, that is used to get the questions
        listView = (ListView)view.findViewById(R.id.f_questions_list);
        // updatefunction to update the graphical user interface
        update();
    }

    /** getListView()
     * Getter Method
     * @return listView
     */

    public ListView getListView() {
        return listView;
    }

    /** update()
     * Is a method that recalls a list, which is developed in getQuestionsFromDb() (Data.java).
     */

    void update() {
        ArrayList<Question> questions = data.getQuestionsFromDb();
        //If the recalled list contains no questions, the if statement starts.
        if(questions.size() <= 0)
        {
            ArrayAdapter adapter = new ArrayAdapter(questionsFragment.getActivity().getApplicationContext(), R.layout.default_list, new String[] {"Es konnten keine Fragen gefunden werden."});
            //The user is showen the text "Es konnten keine Fragen gefunden werden."
            listView.setAdapter(adapter);
            // The command (ask for the question) of the user gets deleted.
            listView.setOnItemClickListener(null);
            return;
        }
        // Includes the list with the questions, that the user gets now to see.
        ArrayAdapter adapter = new ArrayAdapter(questionsFragment.getActivity().getApplicationContext(), R.layout.default_list, questions);
        listView.setAdapter(adapter);
    }
}
