package de.ddoering.rdwquiz.fragments.StatsFragment;

import android.view.View;
import android.widget.ListView;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.data.visualizer.StatsAdapter;

/**
 * @author Peter
 */

/**
 * Anzeige der Statistiken mit ihrem View
 * Generierung der Klasse Ui und Deklaration der Variablen
 */
public class Ui {

    private final StatsFragment statsFragment;
    private final View view;
    private final Data data;

    private final ListView listView;

    /**
     * Aufruf der Ui (laden der Variablen) und Rückgabe des Views
     * @param statsFragment: Übergabe der statsFragment
     * @param view: Übergabe des Views
     * @param data: Übergabe der Data
     */
    public Ui(StatsFragment statsFragment, View view, Data data) {
        this.statsFragment = statsFragment;
        this.view = view;
        this.data = data;

        listView = (ListView)view.findViewById(R.id.f_stats_list);

        StatsAdapter adapter = new StatsAdapter(statsFragment.getActivity().getApplicationContext(), R.layout.default_stats_list, data.getStats());
        listView.setAdapter(adapter);
    }
}
