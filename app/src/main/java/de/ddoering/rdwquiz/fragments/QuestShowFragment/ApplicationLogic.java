package de.ddoering.rdwquiz.fragments.QuestShowFragment;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.Answer;
import de.ddoering.rdwquiz.models.Question;

/**
 * Logic for the questions and answers of multiplechoice and textfiel questions
 * Includes the ApplicationLogic
 *
 * This class is called in the QuestionShowFragment and the EventToListenerMappings
 * of this Fragment.
 *
 * @author awallenborn
 */

public class ApplicationLogic {
    //declaration of questShowFragment and data
    private final QuestShowFragment questShowFragment;
    private final Data data;
    /**
     * creates a instance of this class with the transfer data
     *
     * @param questShowFragment instance of the QuestShowFragment
     * @param data instance of Data
     */
    public ApplicationLogic(QuestShowFragment questShowFragment, Data data) {
        this.questShowFragment = questShowFragment;
        this.data = data;
    }


    /**
     * This method is called when the submittButton is clicked
     * @param v transfer the view in which was clicked the button
     */

    void buttonSubmitQuestionClicked(View v) {
  /*
  submitting the answer of a multiplechoice question
  checks every checkbox if it's checked
  saves the checked boxes as given answer
   */

        if(data.getQuestion().getType().equals(Question.TYPE_MULTI)) {
            //Antwort f�r Multi absenden
            ArrayList<Long> ids = new ArrayList<Long>();
            RelativeLayout container = (RelativeLayout)questShowFragment.getView().findViewById(R.id.f_questshow_answer_container);
            for (int i = 0; i < container.getChildCount(); i++)
            {
                RelativeLayout innerContainer = (RelativeLayout)container.getChildAt(i);
                for(int j = 0; j < innerContainer.getChildCount(); j++)
                {
                    if(innerContainer.getChildAt(j) instanceof CheckBox)
                    {
                        CheckBox checkBox = (CheckBox)innerContainer.getChildAt(j);
                        if(checkBox.isChecked())
                        {
                            Answer answer = (Answer)checkBox.getTag();
                            ids.add(answer.getId());
                        }
                    }
                }
            }
/*
creates a bundle to transfer the given answers
 */

            Bundle data = new Bundle();
            long[] answers = new long[ids.size()];
            for (int i = 0; i< answers.length; i++)
                answers[i] = ids.get(i).longValue();

            data.putLongArray(QuestShowFragment.EXTRA_ANSWER_IDS, answers);
            questShowFragment.getQuestShowInteractionListener().onQuestShowInteraction(QuestShowFragment.ACTION_SUBMITTED, data);
        }
/*
 * submit the answer of textfield questions by creating the bundle with the given answer of the editText
 */
        else {
            String answer = ((EditText) v.getTag()).getText().toString();
            Bundle data = new Bundle();
            data.putString(QuestShowFragment.EXTRA_ANSWER_TEXT, answer);
            questShowFragment.getQuestShowInteractionListener().onQuestShowInteraction(QuestShowFragment.ACTION_SUBMITTED, data);
        }
    }
/*
 * Button which is created by false answering.
 */

    void buttonQuestSeenClicked() {
        questShowFragment.getQuestShowInteractionListener().onQuestShowInteraction(QuestShowFragment.ACTION_QUEST_SEEN, null);
    }
}
