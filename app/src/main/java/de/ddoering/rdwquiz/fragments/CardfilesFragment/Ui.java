package de.ddoering.rdwquiz.fragments.CardfilesFragment;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.fragments.LoginFragment.LoginFragment;
import de.ddoering.rdwquiz.models.Cardfile;

/**
 * This class is the GUI of the CardfilesFragments
 *
 * It is called in the CardfilesFragment and in the EventToListenerMapping.
 *
 * @author czschoer
 */
public class Ui {

    //private CardfilesFragment
    private final CardfilesFragment cardfilesFragment;
    //private View
    private final View view;

    //private ListView
    private final ListView listView;

    /**
     * Creates an instance of this class with delivered data.
     * Updates the GUI.
     *
     * @param view transfer the view
     * @param cardfilesFragment transfer the instance of the cardfilesFragment
     */
    public Ui(View view, CardfilesFragment cardfilesFragment) {
        listView = (ListView) view.findViewById(R.id.f_cardfiles_list);
        this.cardfilesFragment = cardfilesFragment;
        this.view = view;
        updateUi();
    }

    /**
     * getter for the ListView
     *
     * @return gives the listView back
     */
    public ListView getListView() {
        return listView;
    }

    /**
     * Update the GUI.
     * Checks the number of cardfiles and gives the data to the ListView.
     */
    void updateUi() {
        //no cardfile found
        List<Cardfile> cardfiles = Cardfile.listAll(Cardfile.class);
        if(cardfiles.size() <= 0)
        {
            ListAdapter listAdapter = new ArrayAdapter<String>(cardfilesFragment.getActivity().getApplicationContext(), R.layout.default_list, new String[]{"Es konnten keine Karteien gefunden werden"});
            listView.setAdapter(listAdapter);
            return;
        }

        //put data into the list
        ListAdapter listAdapter = new ArrayAdapter<Cardfile>(cardfilesFragment.getActivity().getApplicationContext(), R.layout.default_list, cardfiles);
        listView.setAdapter(listAdapter);
    }
}
