package de.ddoering.rdwquiz.fragments.QuestConfirmFragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.Answer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnQuestConfirmInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuestConfirmFragment#newInstance} factory method to
 * create an instance of this fragment.
 * @author dvogeley
 */
public class QuestConfirmFragment extends Fragment {
    private static final String ARG_GIVEN_ANSWER = "answer";
    private static final String ARG_QUESTION_ID = "questId";
    public static final String ACTION_ANSWER_CORRECT = "correct";
    public static final String ACTION_ANSWER_WRONG = "wrong";

    private String answer;
    private long questId;

    private Ui ui;
    private ApplicationLogic applicationLogic;
    private EventToListenerMapping eventToListenerMapping;
    private Data data;

    private OnQuestConfirmInteractionListener questConfirmationAction;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param answer
     * @param questId
     * @return A new instance of fragment QuestConfirmFragment.
     */
    public static QuestConfirmFragment newInstance(String answer, long questId) {
        QuestConfirmFragment fragment = new QuestConfirmFragment();
        Bundle args = new Bundle();
        args.putString(ARG_GIVEN_ANSWER, answer);
        args.putLong(ARG_QUESTION_ID, questId);
        fragment.setArguments(args);
        return fragment;
    }

    public QuestConfirmFragment() {
        // Required empty public constructor
    }

    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            answer = getArguments().getString(ARG_GIVEN_ANSWER);
            questId = getArguments().getLong(ARG_QUESTION_ID);
        }
    }

    /**
     * Load the helper classes, set title and draw the view
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Korrekt?");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quest_confirm, container, false);

        data = new Data(this);
        ui = new Ui(view, data, this);
        applicationLogic = new ApplicationLogic(this);
        eventToListenerMapping = new EventToListenerMapping(applicationLogic, ui, this);

        return view;
    }

    /**
     * Cast the host activity to Interface to be able to communocate with the host activity
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            questConfirmationAction = (OnQuestConfirmInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnQuestConfirmInteractionListener");
        }
    }

    /**
     * Free ressources
     */
    @Override
    public void onDetach() {
        super.onDetach();
        questConfirmationAction = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnQuestConfirmInteractionListener {
        public void onQuestConfirmInteraction(String what, Bundle data);
    }

    /**
     * Getter for the questId
     * @return questID
     */
    public long getQuestId() {
        return questId;
    }

    /**
     * Getter for the interactionListener
     * @return questConfirmationAction
     */
    public OnQuestConfirmInteractionListener getQuestConfirmationAction() {
        return questConfirmationAction;
    }

    /**
     * Getter for the answer
     * @return answer
     */
    public String getAnswer() {
        return answer;
    }
}
