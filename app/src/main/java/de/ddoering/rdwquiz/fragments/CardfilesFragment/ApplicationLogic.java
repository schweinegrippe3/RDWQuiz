package de.ddoering.rdwquiz.fragments.CardfilesFragment;

import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;

import de.ddoering.rdwquiz.models.Cardfile;

/**
 * Logic fot the CardfileFragment.
 *
 * This class is called in the CardfilesFragments and in the EventToListenerMapping.
 *
 * @author czschoer
 */
public class ApplicationLogic {

    //private Ui
    private final Ui ui;
    //private CardfilesFragment
    private final CardfilesFragment cardfilesFragment;


    /**
     * Creates a new instance of this class with the delivered data.
     *
     * @param ui transfer the GUI for the CardfilesFragment
     * @param cardfilesFragment transfer the instance of the CardfilesFragment
     */
    public ApplicationLogic(Ui ui, CardfilesFragment cardfilesFragment) {
        this.ui = ui;
        this.cardfilesFragment = cardfilesFragment;
    }

    /**
     * This method is executed when a cardfile was clicked.
     * Created are a new cardfile and a new bundle.
     * The bundle saves the cardfile-ID which was clicked and then the ID is delivered to the
     * activity to inform it.
     *
     * @param parent transfer a view which threw the event
     * @param position transfer a position that was clicked
     */
    void CardfileClicked(AdapterView<?> parent, int position) {
        Cardfile cardfile = (Cardfile) parent.getAdapter().getItem(position);
        Bundle data = new Bundle();
        data.putLong(CardfilesFragment.EXTRA_CARDFILE_ID, cardfile.getId());

        //inform the activity
        cardfilesFragment.getCardfilesInteraction().onCardfilesInteraction(CardfilesFragment.ACTION_CARDFILE_CLICKED , data);
    }

    /**
     * This method is called when a cardfile was long clicked.
     * Created are a cardfile and a new bundle.
     * The bundle saves the cardfile-ID which was long clicked and then the ID is delivered to the
     * activity to inform it.
     *
     * @param parent transfer the view which threw the event
     * @param position transfer the position where was long clicked
     */
    void CardfileLongCLicked(AdapterView<?> parent, int position) {
        Cardfile cardfile = (Cardfile) parent.getAdapter().getItem(position);
        //Log.d(TAG, cardfile.getName() + " long clicked");
        Bundle data = new Bundle();
        data.putLong(CardfilesFragment.EXTRA_CARDFILE_ID, cardfile.getId());

        //inform the activity
        cardfilesFragment.getCardfilesInteraction().onCardfilesInteraction(CardfilesFragment.ACTION_CARDFILE_LONG_CLICKED, data);
    }
}
