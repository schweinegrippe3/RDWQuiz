package de.ddoering.rdwquiz.fragments.QuestionsFragment;

import java.util.ArrayList;
import java.util.List;
import de.ddoering.rdwquiz.models.Question;
import de.ddoering.rdwquiz.models.UserQuestion;

/**
 * Data.java is used to administrate the used dates.
 * @author smartens
 */

public class Data {
    // Declaration of the variables: questionsFragment, questions
    private final QuestionsFragment questionsFragment;
    private final ArrayList<Question> questions;

    /** Data()
     * Hand out the variable: questionsFragment
     * Constructor, Initialisation of the variables: questionsFragment, questions
     * @param questionsFragment
     * @return questions
     */

    public Data(QuestionsFragment questionsFragment) {
        this.questionsFragment = questionsFragment;
        this.questions = getQuestionsFromDb();
    }

    // Developing a list of questions, which should be presented to the user.

    ArrayList<Question> getQuestionsFromDb() {
        // Creating a new empty list.
        ArrayList<Question> questions = new ArrayList<>();
        if(questionsFragment.getClassificationId() == 1)
        // The never answered questions should be edited (the one that still doesn't have a classificationId higher one).
        {
            // Definition of the different used parameters.
            String[] args = {String.valueOf(questionsFragment.getUserId()), String.valueOf(questionsFragment.getCardfileId())};
            String query = "Select * from " + Question.getTableName(Question.class) + " where id not in (" +
                    "Select question from " + UserQuestion.getTableName(UserQuestion.class) + " where user = ?) and " +
                    "cardfile = ?";
            List<Question> unknown =  Question.findWithQuery(Question.class, query, args);
            // The variable "unknown" gets connected to the request.
            questions.addAll(unknown);
        }
        // Definition of the different used parameters.
        String[] params = {String.valueOf(questionsFragment.getUserId()), String.valueOf(questionsFragment.getClassificationId()), String.valueOf(questionsFragment.getCardfileId())};
        String query = "select q.* from\n" +
                "" + Question.getTableName(Question.class) + " q, " + UserQuestion.getTableName(UserQuestion.class) + " uq \n" +
                "where uq.question = q.id\n" +
                "and uq.user = ?\n" +
                "and uq.classification = ?\n" +
                "and q.cardfile = ?";
        // The variable "known" gets connected to the request.
        List<Question> known = Question.findWithQuery(Question.class, query, params);
        // The two list/requests were combined in one list.
        questions.addAll(known);
        // Return of the questions, that were asked.
        return questions;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }
}
