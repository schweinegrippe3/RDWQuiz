package de.ddoering.rdwquiz.fragments.QuestionsFragment;

import android.os.Bundle;
import android.widget.AdapterView;
import de.ddoering.rdwquiz.models.Question;

/**
 * The ApplicationLogic contains the functions of this fragment.
 * Declaration of the variables: questionsFragment, ui
 * @author smartens
 */

public class ApplicationLogic {
    private final QuestionsFragment questionsFragment;
    private final Ui ui;

    /** ApplicationLogic()
     * Hand out the variable: questionsFragment, ui
     * Constructor, Initialisation of the variables: questionsFragment, ui
     * @param questionsFragment
     * @param ui
     */

    public ApplicationLogic(QuestionsFragment questionsFragment, Ui ui) {
        this.questionsFragment = questionsFragment;
        this.ui = ui;
    }

    /** OnQuestionsSelected()
     * This method defines what happens, if the user choose a question.
     * The variable q saves the information that a user used an special item to select a question.
     * q is needed for the following process.
     */

    void OnQuestionSelected(AdapterView<?> parent, int position) {
        Question q = (Question) parent.getAdapter().getItem(position);
        Bundle data = new Bundle();
        data.putLong(QuestionsFragment.EXTRA_QUESTION_ID, q.getId());
        questionsFragment.getQuestionsInteractionListener().onQuestionInteraction(QuestionsFragment.ACTION_QUESTION_CLICKED, data);
    }
}
