package de.ddoering.rdwquiz.fragments.StatsFragment;

import java.util.ArrayList;
import java.util.List;

import de.ddoering.rdwquiz.models.Cardfile;
import de.ddoering.rdwquiz.models.Question;
import de.ddoering.rdwquiz.models.Stat;
import de.ddoering.rdwquiz.models.UserQuestion;

/**
 * @author Peter
 */

/**
 * Definition der Statistiken inklusieve ihrer Abfragen
 * Generierung der Klasse Data mit Variablen
 */
public class Data {
    private final StatsFragment statsFragment;
    private final Cardfile cardfile;
    private final long cardfileId;
    private final long userId;

    /**
     * Aufruf der Variablen
     * @param statsFragment: Übergabe
     * @param cardfileId: cardfileId für die Abfrage
     * @param userId: userId für die Abfrage
     */
    public Data(StatsFragment statsFragment, long cardfileId, long userId) {
        this.statsFragment = statsFragment;
        this.cardfile = Cardfile.findById(Cardfile.class, cardfileId);
        this.userId = userId;
        this.cardfileId = cardfileId;
    }

    /**
     * Abfrage der Statik
     * @return: stats
     */
    List<Stat> getStats() {
        List<Stat> stats = new ArrayList<>();

        stats.add(new Stat("Name der Kartei", cardfile.getName()));
        stats.add(new Stat("Version der Kartei", String.valueOf(cardfile.getVersion())));
        stats.add(new Stat("Verfügbare Fragen", String.valueOf(cardfile.getQuestions().size())));
        stats.add(new Stat("Verfügbare Fragen in der 2. Klasse:", String.valueOf(UserQuestion.findWithQuery(UserQuestion.class, "SELECT uq.* FROM " + Question.getTableName(Question.class) + " q, " + UserQuestion.getTableName(UserQuestion.class) + " uq where q.id = uq.question and uq.user = " + getUserId() + " and uq.classification = 2 and q.cardfile = " + getCardfileId()).size())));
        stats.add(new Stat("Verfügbare Fragen in der 3. Klasse:", String.valueOf(UserQuestion.findWithQuery(UserQuestion.class, "SELECT uq.* FROM " + Question.getTableName(Question.class) + " q, " + UserQuestion.getTableName(UserQuestion.class) + " uq where q.id = uq.question and uq.user = " + getUserId() +  " and uq.classification = 3 and q.cardfile = " + getCardfileId()).size())));
        stats.add(new Stat("Verfügbare Fragen in der 4. Klasse:", String.valueOf(UserQuestion.findWithQuery(UserQuestion.class, "SELECT uq.* FROM " + Question.getTableName(Question.class) + " q, " + UserQuestion.getTableName(UserQuestion.class) + " uq where q.id = uq.question and uq.user = " + getUserId() +  " and uq.classification = 4 and q.cardfile = " + getCardfileId()).size())));
        stats.add(new Stat("Verfügbare Fragen in der 5. Klasse:", String.valueOf(UserQuestion.findWithQuery(UserQuestion.class, "SELECT uq.* FROM " + Question.getTableName(Question.class) + " q, " + UserQuestion.getTableName(UserQuestion.class) + " uq where q.id = uq.question and uq.user = " + getUserId() +  " and uq.classification = 5 and q.cardfile = " + getCardfileId()).size())));
        stats.add(new Stat("Verfügbare Fragen in der 6. Klasse:", String.valueOf(UserQuestion.findWithQuery(UserQuestion.class, "SELECT uq.* FROM " + Question.getTableName(Question.class) + " q, " + UserQuestion.getTableName(UserQuestion.class) + " uq where q.id = uq.question and uq.user = " + getUserId() +  " and uq.classification = 6 and q.cardfile = " + getCardfileId()).size())));
        stats.add(new Stat("Verfügbare Fragen in der 7. Klasse:", String.valueOf(UserQuestion.findWithQuery(UserQuestion.class, "SELECT uq.* FROM " + Question.getTableName(Question.class) + " q, " + UserQuestion.getTableName(UserQuestion.class) + " uq where q.id = uq.question and uq.user = " + getUserId() +  " and uq.classification = 7 and q.cardfile = " + getCardfileId()).size())));
        return stats;
    }

    /**
     * Rückgabe
     * @return: cardfile
     */
    public Cardfile getCardfile() {
        return cardfile;
    }

    /**
     * Rückgabe
     * @return: cardfileId
     */
    public long getCardfileId() {
        return cardfileId;
    }

    /**
     * Rückgabe
     * @return: userId
     */
    public long getUserId() {
        return userId;
    }
}
