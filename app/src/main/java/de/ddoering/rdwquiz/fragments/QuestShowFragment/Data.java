package de.ddoering.rdwquiz.fragments.QuestShowFragment;

import de.ddoering.rdwquiz.models.Question;

/**
 * @author Peter
 */

/**
 * laden der Fragen anhand der ID und der Klasse
 * Generierung der Klasse Data
 * Deklaration der Variablen
 */
public class Data {

    private final QuestShowFragment questShowFragment;
    private final Question question;

    /**
     * Initialisierung der Variablen
     * @param questShowFragment: aus questShowFragment
     */
    public Data(QuestShowFragment questShowFragment) {
        this.questShowFragment = questShowFragment;
        this.question = Question.findById(Question.class, questShowFragment.getQuestionId());
    }

    /**
     * Rückgabe
     * @return: question
     */
    public Question getQuestion() {
        return question;
    }
}
