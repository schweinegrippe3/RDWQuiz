package de.ddoering.rdwquiz.fragments.QuestShowFragment;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.Answer;
import de.ddoering.rdwquiz.models.Question;

/**
 * @author Peter  @author anne
 */

/**
 * Anzeige des Backgrounds und der jeweiligen Möglichkeiten zur Eingabe der Antwort
 * Generierung der Klasse Ui
 * Deklaration der Variablen
 */
public class Ui {

    private final QuestShowFragment questShowFragment;
    private final Data data;

    private final TextView textView;
    private final Button button;

    /**
     * Initialisierung der Variablen und Wiedergabe des textView, button
     * @param questShowFragment: Aufruf aus questShowFragment
     * @param data: frage
     * @param view: anzeige
     */
    public Ui(QuestShowFragment questShowFragment, Data data, View view) {
        this.questShowFragment = questShowFragment;
        this.data = data;

        textView = (TextView)view.findViewById(R.id.f_questshow_question_text);
        button = (Button)view.findViewById(R.id.f_questshow_submit_button);

        textView.setText(data.getQuestion().getTitle());


        /**
         * Vorbereitung der Frage
         */
        if(data.getQuestion().getType().equals(Question.TYPE_TEXT) || data.getQuestion().getType().equals(Question.TYPE_TEXT_FREE) || data.getQuestion().getType().equals(Question.TYPE_NUMBER))
        {
            prepareForText(data.getQuestion(), view);
        }
        if(data.getQuestion().getType().equals(Question.TYPE_MULTI))
        {
            prepareForMulti(data.getQuestion(), view);
        }

        /**
         * Generierung des Backgrounds anhand der Frage
         */
        switch(data.getQuestion().getCardfile().getName().toLowerCase()){
            case "algen":
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.algenhi);
                break;
            case "delfine":
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.delfinehi);
                break;
            case "haie":
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.haihi);
                break;
            case "korallen":
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.korallenhi);
                break;
            case "rochen":
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.rochenhi);
                break;
            case "schildkröten":
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.schildkroetenhi);
                break;
            case "seepferdchen":
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.seepferdchenhi);
                break;
            case "wale":
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.walhi);
                break;
            default:
                view.findViewById(R.id.fragment_quest_showID).setBackgroundResource(R.drawable.blasenhi);

        }
    }
//Autor : Anne Container layout und Logik für TExt und Ankreuzfragen
    private void prepareForMulti(Question question, final View view) {              //Vorbereitung des Fragments für Ankreuzfragen
        String query = "select * from " + Answer.getTableName(Answer.class) + " where question = ?"; //Antworten zur gewählten Frage Laden
        List<Answer> answers = Answer.findWithQuery(Answer.class, query, question.getId().toString());
        RelativeLayout relativeLayout = (RelativeLayout)(view.findViewById(R.id.f_questshow_answer_container)); //Antworten als List in AntwortContainer laden

        ArrayList<RelativeLayout> answerContainers = new ArrayList<>();
        int i = 1;
        for (Answer answer : answers)
        {
            RelativeLayout container = new RelativeLayout(questShowFragment.getActivity().getApplicationContext());
            RelativeLayout.LayoutParams containerParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            container.setId(i);
            if(i > 1)
                containerParams.addRule(RelativeLayout.BELOW, i-1);
            container.setPadding(0, 0, 0, 18);
            container.setLayoutParams(containerParams);
            container.setGravity(Gravity.CENTER_VERTICAL);

            CheckBox checkBox = new CheckBox(questShowFragment.getActivity().getApplicationContext());
            checkBox.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            checkBox.setId(i + 100);
            checkBox.setTag(answer);
            container.addView(checkBox);
            if(questShowFragment.isReadOnly()) {
                checkBox.setEnabled(false);
                if(answer.isCorrect())
                {
                    checkBox.setChecked(true);
                }
            }

            //TODO Warum zum fick wird die nicht gecentert xO
            TextView textView = new TextView(questShowFragment.getActivity().getApplicationContext());
            RelativeLayout.LayoutParams tvParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            tvParams.addRule(RelativeLayout.RIGHT_OF, i+100);
            textView.setLayoutParams(tvParams);
            textView.setText(answer.getTitle());
            textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
            textView.setTypeface(null, Typeface.BOLD);
            textView.setTextSize(24);
            textView.setTextColor(Color.WHITE);
            container.addView(textView);

            answerContainers.add(container);
            i++;
        }

        for (RelativeLayout r : answerContainers)
            relativeLayout.addView(r);

        Button button = (Button)(view.findViewById(R.id.f_questshow_submit_button));

        if(questShowFragment.isReadOnly())
            button.setText("OK");
    }

    /**
     * Container für Textfragen
     * @param q: jeweilige Frage
     * @param view: view der Frage
     */
    public void prepareForText(Question q, View view){
        RelativeLayout relativeLayout = (RelativeLayout)(view.findViewById(R.id.f_questshow_answer_container));
        relativeLayout.setGravity(Gravity.BOTTOM);

        EditText editText = new EditText(questShowFragment.getActivity().getApplicationContext());
        editText.setTextColor(Color.WHITE);
        editText.setLayoutParams(new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        if(q.getType().equals(Question.TYPE_NUMBER))
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);

        Button button = (Button)(view.findViewById(R.id.f_questshow_submit_button));
        button.setTag(editText);

        if(questShowFragment.isReadOnly()) {
            for(Answer a : q.getAnswers())
                editText.setText(editText.getText() + "\n" + "\u2022" + " " + a.getTitle());
            editText.setEnabled(false);
        } else if(q.getType().equals(Question.TYPE_TEXT_FREE))
        {
            editText.setVisibility(View.INVISIBLE);
            TextView tv = (TextView)view.findViewById(R.id.f_questshow_question_text);
            tv.setText("Machen Sie sich Gedanken zu folgender Frage:\n" + tv.getText());
            button.setText("Fertig");
        }

        relativeLayout.addView(editText);

        if(questShowFragment.isReadOnly())
            button.setText("OK");

    }

    /**
     * Rückgabe
     * @return: button
     */
    public Button getButton() {
        return button;
    }

    /**
     * Rückgabe
     * @return: textView
     */
    public TextView getTextView() {
        return textView;
    }
}
