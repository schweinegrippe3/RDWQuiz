package de.ddoering.rdwquiz.fragments.ClassificationsFragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;

import de.ddoering.rdwquiz.R;

/**
<<<<<<< HEAD
 * This fragment creates all views, interactions and logic for the Classification of the questions
 *
 * This fragment calls the ApplicationLogic, EventToListenerMapping and the UI.
 * It is called in the QuizActivity.
 *
 * @author awallenborn
=======
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnClassificationsInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ClassificationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 * @author ddoering
>>>>>>> remotes/origin/DV-Kommentare
 */
public class ClassificationsFragment extends Fragment {
    //String for the cardfileId
    private static final String ARG_CARDFILE_ID = "cardfileId";
    //String for the UserId
    private static final String ARG_USER_ID = "userId";
    //String for the action of selecting a class
    public static final String ACTION_CLASSIFICATION_SELECTED = "select";
    //String for th
    public static final String EXTRA_CLASSIFICATION = "classification";

    private Ui ui;
    private ApplicationLogic applicationLogic;
    private EventToListenerMapping eventToListenerMapping;
    private Data data;

    private long cardfileId;
    private long userId;

    private OnClassificationsInteractionListener classificationsInteractionListener;

    /**
     * Creates a new Instance with the delivered cardfileId and userId
     * UserId and cardfileId are saved in a bundle
     * returns an instance of this fragment
     */
    public static ClassificationsFragment newInstance(long cardfileId, long userId) {
        ClassificationsFragment fragment = new ClassificationsFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_CARDFILE_ID, cardfileId);
        args.putLong(ARG_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }
    // creates an empty instance of this fragment
    public ClassificationsFragment() {
        // Required empty public constructor
    }

    /*
     * Called to do initial creation of the fragment. Is called before onCreateView().
     * Method is override. Saves the cardfileId and the UserId out of the bundle if the arguments are not null.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cardfileId = getArguments().getLong(ARG_CARDFILE_ID);
            userId = getArguments().getLong(ARG_USER_ID);
        }
    }

    /**
     * In this method is a view created
     * Creates a GUI and transfer to it the view and the instance of this fragment.
     * The ApplicationLogic is created and gets transferred the GUI and the instance of this fragment.
     * As well the EventList is created with the ApplicationLogic and the GUI.
     *
     * @param inflater is used to fill the view of the fragment
     * @param container is used to generate layout parameters
     * @param savedInstanceState this fragments is returned to older status if not null
     * @return gives a view of the classification back
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_classifications, container, false);

        data = new Data(this);
        ui = new Ui(this, view, data);
        applicationLogic = new ApplicationLogic(this);
        eventToListenerMapping = new EventToListenerMapping(applicationLogic, ui);

        return view;
    }

    /**
     * This method is executed when the fragment is first attached to the activity.
     * Must be called before the fragment can be created.
     * It checks if the FragmentInteraction is implemented else a exception is threw.
     * Puts the fragmentInteraction on the delivered activity.
     *
     * @param activity transfer the activity to which the connection should be
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            classificationsInteractionListener = (OnClassificationsInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnClassificationsInteractionListener");
        }
    }

    /**
     * must be implemented to communicate with the activity and other fragments that are connected
     * to the activity.
     */
    public interface OnClassificationsInteractionListener {
        public void onClassificationsInteraction(String what, Bundle data);
    }
    // getter for the UserId
    public long getUserId() {
        return userId;
    }
    //getter for the CardfileId
    public long getCardfileId() {
        return cardfileId;
    }
    /**
     *  getter for the classificationInteraction
     *
     * @return gives the classificationInteraction back
     */
    public OnClassificationsInteractionListener getClassificationsInteractionListener() {
        return classificationsInteractionListener;
    }
}
