package de.ddoering.rdwquiz.fragments.QuestionsFragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import de.ddoering.rdwquiz.R;

/**
 * QuestionsFragment.java is a fragment to list the questions.
 * @author smartens
 */

public class QuestionsFragment extends Fragment {

    /**
     * Definition of the references: userId, classificationId, cardfileId, questionClick, questionId
     * The references should always refer to the same objects.
     */

    private static final String ARG_USER_ID = "userId";
    private static final String ARG_CLASSIFICATION_ID = "classificationId";
    private static final String ARG_CARDFILE_ID = "cardfileId";
    public static final String ACTION_QUESTION_CLICKED = "questionClicked";
    public static final String EXTRA_QUESTION_ID = "questionId";

    /**
     * Declaration of the long variables: userId, classificaitonId, cardfileId
     * The ID of the user is saved in the userId.
     * The ID of the classification (questions are saved in) is saved in the classificationId.
     * The ID of the category is saved in the cardfileId.
     */

    private long userId;
    private long classificationId;
    private long cardfileId;

    /**
     * Declaration of the variables: ui, applicationLogic, eventToListenerMapping, data, questionsInteractionListener
     * ui: contains everything that is connected to the interface.
     * applicationLogic: to separate everything that contains logic in the fragment (for example the actions)
     * eventToListenerMapping: is the connection between the ui and the applicationLogic (defines, what happens when the user choose a question)
     * data: data management
     * questionsInteractoinListener: is an interface to support the communication with the hostactivity
     */

    private Ui ui;
    private ApplicationLogic applicationLogic;
    private EventToListenerMapping eventToListenerMapping;
    private Data data;
    private OnQuestionsInteractionListener questionsInteractionListener;

    /** newInstance()
     * Generating a new instance of the fragment, while using the parameters: userId, cardfileId, classificationId
     * The return is a new instance of questionsFragment.
     * @param userId Parameter 1.
     * @param cardfileId Parameter 2.
     * @param classificationId Parameter 3.
     * @return fragment
     */

    public static QuestionsFragment newInstance(long userId, long cardfileId, long classificationId) {
        // Generate a new variable "fragment" of the type QuestionsFragment
        QuestionsFragment fragment = new QuestionsFragment();
        // Generate a bundle with the name "args"
        Bundle args = new Bundle();
        // Assign the different IDs with the appropriate bundle
        args.putLong(ARG_USER_ID, userId);
        args.putLong(ARG_CLASSIFICATION_ID, classificationId);
        args.putLong(ARG_CARDFILE_ID, cardfileId);
        fragment.setArguments(args);
        return fragment;
    }

    /** QuestionsFragment()
     * automatically generated empty constructor
     */

    public QuestionsFragment() {
    }

    /** onCreate()
     * Method of a fragment, that checks if there are datas saved in the bundle. If there are datas, the userId, classificationId and the
     * cardfileId were edit.
     * @param savedInstanceState
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getLong(ARG_USER_ID);
            classificationId = getArguments().getLong(ARG_CLASSIFICATION_ID);
            cardfileId = getArguments().getLong(ARG_CARDFILE_ID);
        }
    }

    /** onCreateView()
     * Override of the method.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflate the layout for this fragment.
        View view = inflater.inflate(R.layout.fragment_questions, container, false);

        //Initialisation of the variables.
        data = new Data(this);
        ui = new Ui(view, this, data);
        applicationLogic = new ApplicationLogic(this, ui);
        eventToListenerMapping = new EventToListenerMapping(ui, applicationLogic, data);
        return view;
    }

    /** onAttach()
     * The questionsInteractionListener gets assigned to the activity.
     * The try- and catch-Block stops an exception, if the assign fails.
     * @param activity
     */

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // hostactivity gets castet to an interface (now a type of OnQuestionsInteractionListener)
            questionsInteractionListener = (OnQuestionsInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnQuestionsInteractionListener");
                    //message, that it did not work
        }
    }

    /** onDetach()
     * Call for the method onDetach.
     * With the super-class the method inherits every functionality of the fathermethod.
     * The variable questionsInteractionListener gets turned "null", to save resources.
     */

    @Override
    public void onDetach() {
        super.onDetach();
        questionsInteractionListener = null;
    }

    /** updateQuestions()
     * Call of the method "update" out of the document Ui.java.
     * Layout gets updated.
     */

    public void updateQuestions() {
        ui.update();
    }

    /** OnQuestionsInteractionListener()
     * Interface is used for the communication with the hostactivity.
     */

    public interface OnQuestionsInteractionListener {
        public void onQuestionInteraction(String what, Bundle data);
    }

    /** getUserId()
     * Getter Method
     * @return userId
     */

    public long getUserId() {
        return userId;
    }

    /** getClassificationId()
     * Getter Method
     * @return classificationId
     */

    public long getClassificationId() {
        return classificationId;
    }

    /** getCardfileId()
     * Getter Method
     * @return cardfileId
     */

    public long getCardfileId() {
        return cardfileId;
    }

    /** getQuestionsInteractionListener()
     * Getter Method for the up used interface.
     * @return questionsInteractionListener
     */

    public OnQuestionsInteractionListener getQuestionsInteractionListener() {
        return questionsInteractionListener;
    }
}
