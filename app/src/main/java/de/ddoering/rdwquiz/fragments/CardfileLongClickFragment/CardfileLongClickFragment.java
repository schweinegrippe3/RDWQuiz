package de.ddoering.rdwquiz.fragments.CardfileLongClickFragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import de.ddoering.rdwquiz.FragmentInteraction;
import de.ddoering.rdwquiz.R;

/**
 * Simple fragment. Provides a context menu which opens when there was a long click on a cardfile.
 *
 * In this fragment are the applicationLogic, EventToListenerMapping and UI called.
 * This fragment is called by the QuizActivity.
 *
 * @author czschoer
 */
public class CardfileLongClickFragment extends Fragment {
    //String for the cardfile-ID argument
    public static final String ARG_CARDFILE_ID = "cardfileId";
    //String for the action to start the quiz
    public static final String ACTION_CONTEXT_START_QUIZZ = "startQuizzing";
    //String for the action to share a cardfile
    public static final String ACTION_CONTEXT_SHARE = "startShare";
    //String for the action to show the statistics
    public static final String ACTION_CONTEXT_SHOW_STATS = "showStats";
    //String for the cardfile-ID
    public static final String EXTRA_CARDFILE_ID = "cardfileId";
    //String for the order
    public static final String EXTRA_ORDER_BY = "orderBy";

    //private long number fpr the cardfile-ID
    private long cardfileId;

    //private ApplicationLogic
    private ApplicationLogic applicationLogic;
    //private EventToListenerMapping
    private EventToListenerMapping eventToListenerMapping;
    //private Ui
    private Ui ui;

    //private FragmentInteraction
    private FragmentInteraction fragmentInteraction;

    /**
     * Factory method.
     * Create a instance of this fragment with the transferred data.
     *
     * @param cardfileId long the cardfile-ID which was long clicked
     * @return a new instance of the CardfileLongClickFragment
     */
    public static CardfileLongClickFragment newInstance(long cardfileId) {
        CardfileLongClickFragment fragment = new CardfileLongClickFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_CARDFILE_ID, cardfileId);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Create a new empty instance of this fragment
     */
    public CardfileLongClickFragment() {
        // Required empty public constructor
    }

    /**
     * Transfer the saved username out of the bundle savedInstanceState, if the
     * nuber or arguments are not null.
     *
     * @param savedInstanceState bundle with information about the cardfile-ID
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cardfileId = getArguments().getLong(ARG_CARDFILE_ID);
        }
    }

    /**
     * This method creates a view.
     * A GUI is created. The created view and the instance of this fragment is assigned.
     * To the created ApplicationLogic are the GUI and the instance assigned.
     * For the created EventToListenerMapping are the instance, ApplicationLogic and GUI assigned.
     *
     * @param inflater LayoutInflater is used to fill the view of this fragment
     * @param container ViewGroup is used to generate layout parameter
     * @param savedInstanceState bundle this fragment is returned to a previous state if not null
     * @return gives a view for the cardfiles back
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cardfile_long_click, container, false);

        applicationLogic = new ApplicationLogic(this);
        ui = new Ui(view, this);
        eventToListenerMapping = new EventToListenerMapping(this, ui, applicationLogic);

        return view;
    }


    /**
     * This method is called when the fragment is first called to a activity.
     * Must be executed before the fragments can be created.
     * It checks if the FragmentInteraction is implemented otherwise a exception is thrown.
     * Puts the fragmentInteraction on the transferred activity.
     *
     * @param activity transfer an activity to which the connection should be
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragmentInteraction = (FragmentInteraction) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentInteraction");
        }
    }

    /**
     * This method is executed as soon as the fragment don't belong anymore to the activity.
     * It release memory cell.
     * Is called after onDestroyed().
     * Puts the fragmentInteraction on null.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        fragmentInteraction = null;
    }

    /**
     *  getter for the cardfile-ID
     *
     * @return gives the cardfile-ID back
     */
    public long getCardfileId() {
        return cardfileId;
    }

    /**
     * getter for the FragmentInteraction
     *
     * @return gives the interactions of this fragment back
     */
    public FragmentInteraction getFragmentInteraction() {
        return fragmentInteraction;
    }
}
