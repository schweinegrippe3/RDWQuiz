package de.ddoering.rdwquiz.fragments.ClassificationsFragment;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.Question;
import de.ddoering.rdwquiz.models.UserQuestion;

/**
 * Container for the Ui inside the ClassificationsFragment
 * @author ddoering
 */
public class Ui {
    //Used vars
    private final ClassificationsFragment classificationsFragment;
    private final ListView listView;
    private final Data data;

    /**
     * Constructor. Going to define the used vars and set the content into the UI.
     * @param classificationsFragment Istance of the host fragment
     * @param view view which should be used to find objects and set the content
     * @param data Data instance to get the required data
     */
    public Ui(ClassificationsFragment classificationsFragment, View view, Data data) {
        this.classificationsFragment = classificationsFragment;
        this.data = data;
        listView = (ListView)view.findViewById(R.id.f_classifications_list);

        //set content
        setContent();
    }

    /**
     * This method should be used to set the content.
     */
    private void setContent() {
        String[] classes = {"Klasse 1", "Klasse 2", "Klasse 3", "Klasse 4", "Klasse 5", "Klasse 6", "Klasse 7"};


        //Fragen zu den Klassen zaehlen
        for (int i = 1; i <= 7; i++)
        {
            if(data != null)
                classes[i-1] = classes[i-1] + "\n" + data.questCount(i) + " Fragen";
        }

        ArrayAdapter adapter = new ArrayAdapter(classificationsFragment.getActivity().getApplicationContext(), R.layout.default_list, classes);
        listView.setAdapter(adapter);
    }

    /**
     * getter for the ListView inside the Ui.
     * @return ListView, containing the Classifications.
     */
    public ListView getListView() {
        return listView;
    }
}
