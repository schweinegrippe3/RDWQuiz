package de.ddoering.rdwquiz.fragments.CardfileLongClickFragment;

import android.os.Bundle;

import de.ddoering.rdwquiz.fragments.QuestShowFragment.Data;

/**
 * Logic fpr the Fragment with a long click on a cardfile
 * Includes the ApplicationLogic
 *
 * This class is called in the CardfileLongClickFragments and the EventToListenerMappings
 * of this Fragment.
 *
 * @author czschoer
 */
public class ApplicationLogic {
    //private CardfileLongClickFragment
    private final CardfileLongClickFragment cardfileLongClickFragment;

    /**
     * creates a instance of this class with the transfer data
     *
     * @param cardfileLongClickFragment instance of the CardfileLongClickFragment
     */
    public ApplicationLogic(CardfileLongClickFragment cardfileLongClickFragment) {
        this.cardfileLongClickFragment = cardfileLongClickFragment;
    }

    /**
     * This method checks if the quiz should be order by alphabet or by random.
     * Create a bundle and transfer the cardfile-ID.
     *
     * @param position int transfer the position where was clicked
     */
    void onQuestStartClicked(int position) {
        Bundle data = new Bundle();
        data.putLong(CardfileLongClickFragment.EXTRA_CARDFILE_ID, cardfileLongClickFragment.getCardfileId());

        data.putString(CardfileLongClickFragment.EXTRA_ORDER_BY, (position == 0 ? "title" : "RANDOM()"));
        cardfileLongClickFragment.getFragmentInteraction().onFragmentInteraction(CardfileLongClickFragment.ACTION_CONTEXT_START_QUIZZ, data);
    }

    /**
     * Is executed when after a long click on a cardfile the statistics are chosen.
     * Create a new bundle where the cardfile-ID is saved to call it later.
     */
    void onStatsClicked() {
        Bundle data = new Bundle();
        data.putLong(CardfileLongClickFragment.EXTRA_CARDFILE_ID, cardfileLongClickFragment.getCardfileId());
        cardfileLongClickFragment.getFragmentInteraction().onFragmentInteraction(CardfileLongClickFragment.ACTION_CONTEXT_SHOW_STATS, data);
    }
}
