package de.ddoering.rdwquiz.fragments.LoginFragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import de.ddoering.rdwquiz.R;

/**
 *
 * @author cwagner
 */
public class EventToListenerMapping implements View.OnClickListener, AdapterView.OnItemSelectedListener, TextWatcher{

    private final ApplicationLogic applicationLogic;
    private final Ui ui;

    /**
     *   Sorgt dafür, dass den Buttons und Textfeldern aus der View ein Listener zugeordnet wird, um Aktionen des Users bemerken und abfangen zu können
     *   @param ui Übergabe der gewünschten UI aus denen die Elemente stammen
     *   @param applicationLogic dahinterliegende Logik
     */
    public EventToListenerMapping(Ui ui, ApplicationLogic applicationLogic) {
        ui.getBtnLogin().setOnClickListener(this);
        ui.getBtnQuit().setOnClickListener(this);
        ui.getEtUsername().addTextChangedListener(this);
        ui.getUsernameSpinner().setOnItemSelectedListener(this);
        this.applicationLogic = applicationLogic;
        this.ui = ui;
    }


    @Override
    /**
     * Methode fängt Klicks des Users auf dem UI ab und regelt den Umgang mit diesen
     * Zwei Aktionen sind denkbar: User klickt beenden oder anmelden
     * Beenden: Beenden der App über entsprechende Methode in der Logik
     * Anmelden: Benutzer anmelden und weiter zu den Karteien über entsprechende Methode der Logik samt Übergabe des Benutzernamens
     * @param view Betreffende view muss übergeben werden
     */
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.f_login_loginButton:
                applicationLogic.btnLoginClicked(ui.getEtUsername());
                break;
            case R.id.f_login_quitButton:
                applicationLogic.btnQuitClicked();
                break;
        }
    }

    @Override
    /**
     * Methode reagiert auf Benutzerauswahl im Spinner und gibt dies an die Logik weiter
     */
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        applicationLogic.userSelected(parent, position);
    }

    /**
     * Benutzer hat nichts im Spinner ausgewählt
     * @param parent
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        return;
    }

    @Override
    /**
     * Standard-Ansicht im Textfeld für neuen Benutzernamen
     */
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        return;
    }

    /**
     * Benutzer ändert Text des Textfelds für neuen Benutzernamen
     * @param s Standardtextansicht des textfeldes
     * @param start Startposition der Eingabe des Nutzers
     * @param before Text vor Benutzereingabe
     * @param count Zählen der Länge der Benutzereingabe
     */
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        return;
    }

    /**
     * Methode gibt Nutzereingabe für Benutzername an die Logic zur Verarbeitung weiter
     * @param s Benutzereingabe übergeben
     */
    @Override
    public void afterTextChanged(Editable s) {
        applicationLogic.onEtUsernameChanged(s);
    }
}