package de.ddoering.rdwquiz.fragments.LoginFragment;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import de.ddoering.rdwquiz.MainActivity;
import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.User;

/**
 * Erstellung eines Fragments für den Login. Das Fragment liefert Methoden zur Erstellung der UI (View), dem Einbetten des Fragments in seinen Kontext und Anweisungen zum Umgang mit Fortsetzungen der App
 * @author cwagner
 *
 */
public class LoginFragment extends Fragment {

    public final static String ACTION_LOGIN = "login";
    public final static String ACTION_QUIT = "quit";

    private OnLoginInteractionListener interactionListener;

    //Deklarieren benötigter Variablen: Eigenes UI für das Fragment, Umgang mit Benutzeraktionen (Events) und die Logik dahinter
    private Ui ui;
    private EventToListenerMapping eventToListenerMapping;
    private ApplicationLogic applicationLogic;

    public LoginFragment() {
        // leerer Konstruktor wird von einem Fragment vorausgesetzt
    }

    /**
     * Wird aufgerufen, wenn das Fragment das erste Mal aufgerufen wird, erstellt dabei das UI
     * @param inflater: Wird verwendet, um das Layout aus einer XML-Ressource zu generieren (hier: login_fragment.xml)
     * @param container: Relevant für die Übergabe von Paramtern für das Layout, spezifiziert durch die View
     * @param savedInstanceState: Sichert den Stand des Fragments
     * @return View - Gibt die View hierarchie wieder von Fragment innerhalb des Layouts
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Generiert das Layout aus der XML-Datei und liefert die View
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        // erstellt das UI, die dazugehörige Logik und den Eventlistener
        ui = new Ui(view, this);
        applicationLogic = new ApplicationLogic(this, ui);
        eventToListenerMapping = new EventToListenerMapping(ui, applicationLogic);

        getActivity().setTitle("Anmelden");

        return view;
    }

    /**
     * Methode sorgt für Implementieren des Interfaces in aufrufender Activity und lädt den Listener in den interactionListener
     * @param activity aufrufende Activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            interactionListener = (OnLoginInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginInteractionListener");
        }
    }

    /**
     * Sorgt dafür das der User mit dem Interface interagieren kann, welches resumed wird, wenn also wieder darauf zurückgegriffen wird nachdem es bereits geladen wurde
     */
    @Override
    public void onResume() {
        super.onResume();
        ui.updateUi();
    }

    /**
     *
     * @return gibt den interactionListener für Login zurück
     */
    public OnLoginInteractionListener getInteractionListener() {
        return interactionListener;
    }

    /**
     * Interface muss von Klassen implementiert werden, die dieses Fragment verwenden
     * Ziel: Sicherstellung der Kommunikation zwischen Fragment und Activity
     */
    public interface OnLoginInteractionListener {
        public void onLoginInteraction(String what, Bundle data);
    }


}
