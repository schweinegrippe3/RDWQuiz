package de.ddoering.rdwquiz.fragments.QuestShowFragment;

import android.view.View;

/**
 * Events that are used while answering the multiplechoice and textfield question
 *
 * This class is called in the QuestionShowFragment.
 *
 * @author awallenborn
 */
public class EventToListenerMapping implements View.OnClickListener {

    /*
    declaration of questShowFragment, applicationLogic and ui
     */
    private final QuestShowFragment questShowFragment;
    private final ApplicationLogic applicationLogic;
    private final Ui ui;

    /**
     * Creates a instance of this class with the transferred data.
     *
     * @param questShowFragment transfer a instance of the QuestShowFragment
     * @param ui transfer the GUI of the QuestShowFragment
     * @param applicationLogic transfer the applicationLogic ot the QuestShowFragment
     */
    public EventToListenerMapping(QuestShowFragment questShowFragment, ApplicationLogic applicationLogic, Ui ui) {
        this.questShowFragment = questShowFragment;
        this.applicationLogic = applicationLogic;
        this.ui = ui;

        ui.getButton().setOnClickListener(this);
    }
    /**
     * This method is called when the submitt button is clicked.
     * Theres is an differation which function the button has
     * @param v transfer the view in which was clicked long
     */
    @Override
    public void onClick(View v) {
        if(questShowFragment.isReadOnly())
            applicationLogic.buttonQuestSeenClicked();
        else
            applicationLogic.buttonSubmitQuestionClicked(v);
    }
}
