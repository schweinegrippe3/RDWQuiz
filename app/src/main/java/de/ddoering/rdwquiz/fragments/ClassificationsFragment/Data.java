package de.ddoering.rdwquiz.fragments.ClassificationsFragment;

import java.util.List;

import de.ddoering.rdwquiz.models.Question;
import de.ddoering.rdwquiz.models.UserQuestion;

/**
 * Container for the Data logic inside the ClassificationsFragment
 * @author ddoering
 */
public class Data {

    //Instance of the host fragment
    private final ClassificationsFragment classificationsFragment;

    /**
     * Constructor. Sets the used vars
     * @param classificationsFragment Instance of the host fragment
     */
    public Data(ClassificationsFragment classificationsFragment) {
        this.classificationsFragment = classificationsFragment;
    }

    /**
     * Count questions inside the classification
     * @param classification Classification which should be used
     * @return Number of questions inside the classification
     */
    int questCount(int classification) {
        int unknown = 0;

        //Some questions may not have a UserQuestion dataset (never answered questions). Letś get them...
        if(classification == 1)
        {
            String query = "Select * from " + Question.getTableName(Question.class) + " where id not in (" +
                    "Select question from " + UserQuestion.getTableName(UserQuestion.class) + " where user = ?) and " +
                    "cardfile = ?";

            List<Question> newQuestions =  Question.findWithQuery(Question.class, query, String.valueOf(classificationsFragment.getUserId()), String.valueOf(classificationsFragment.getCardfileId()));
            unknown = newQuestions.size();
        }

        //Join Questions with UserQuestions. Use where clause to calculate result.
        String query = "Select uq.* from " + UserQuestion.getTableName(UserQuestion.class) + " uq, " + Question.getTableName(Question.class) + " q " +
                "where uq.question = q.id and uq.classification = ? and q.cardfile = ? and uq.user = ?";

        //Prepare parameters for foin and where statement
        String[] params = {String.valueOf(classification), String.valueOf(classificationsFragment.getCardfileId()), String.valueOf(classificationsFragment.getUserId())};
        //Fire!
        List<UserQuestion> known = UserQuestion.findWithQuery(UserQuestion.class, query, params);

        return unknown + known.size();
    }
}
