package de.ddoering.rdwquiz.fragments.CardfileLongClickFragment;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import de.ddoering.rdwquiz.R;

/**
 * This class generate the GUI of the CardfilesLongClickFragments
 *
 * It is called in the CardfilesLongClickFragment and in the EventToListenerMapping.
 *
 * @author czschoer
 */
public class Ui {

    //private view
    private final View view;
    //private CardfileLongClickFragment
    private final CardfileLongClickFragment cardfileLongClickFragment;
    //private finale ListView
    private final ListView listView;

    /**
     * Creates a new instance of this class with the delivered data.
     * The listing of the menu for the actions with a long click are defined.
     *
     * @param view transfer a view
     * @param cardfileLongClickFragment transfer a instance of the CardfileLongClickFragment
     */
    public Ui(View view, CardfileLongClickFragment cardfileLongClickFragment) {
        this.view = view;
        this.cardfileLongClickFragment = cardfileLongClickFragment;

        this.listView = (ListView)view.findViewById(R.id.f_context_list);

        String[] actions = {"Abfragen (alphabetisch)", "Abfragen (unsortiert)" ,"Statistiken anzeigen"};
        ArrayAdapter adapter = new ArrayAdapter(cardfileLongClickFragment.getActivity().getApplicationContext(), R.layout.default_list, actions);
        listView.setAdapter(adapter);
    }

    /**
     * getter for the ListView
     *
     * @return gives the ListView back
     */
    public ListView getListView() {
        return listView;
    }
}
