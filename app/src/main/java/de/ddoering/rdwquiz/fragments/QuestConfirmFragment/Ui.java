package de.ddoering.rdwquiz.fragments.QuestConfirmFragment;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import de.ddoering.rdwquiz.R;

/**
 * Container for the UI of the QuestConfirmFragment
 * @author dvogeley
 */
public class Ui {
    //Used vars
    private final TextView correctTv;
    private final TextView givenTv;
    private final Button buttonCorrect;
    private final Button buttonWrong;
    private final Data data;
    private final QuestConfirmFragment questConfirmFragment;

    /**
     * KConstructor. Set used vars and set content
     * @param view
     * @param data
     * @param questConfirmFragment
     */
    public Ui(View view, Data data, QuestConfirmFragment questConfirmFragment) {
        this.data = data;
        this.questConfirmFragment = questConfirmFragment;
        correctTv = (TextView)(view.findViewById(R.id.f_confirm_tv_correct));
        givenTv = (TextView)(view.findViewById(R.id.f_confirm_tv_user));
        buttonCorrect = (Button)view.findViewById(R.id.f_confirm_button_yes);
        buttonWrong = (Button)view.findViewById(R.id.f_confirm_button_no);

        //set content
        correctTv.setText(data.getCorrectAnswer().getTitle());
        givenTv.setText(questConfirmFragment.getAnswer());
    }

    /**
     * Getter for buttonWrong
     * @return buttonWrong
     */
    public Button getButtonWrong() {
        return buttonWrong;
    }

    /**
     * Getter for buttonCorrect
     * @return buttonCorrect
     */
    public Button getButtonCorrect() {
        return buttonCorrect;
    }
}
