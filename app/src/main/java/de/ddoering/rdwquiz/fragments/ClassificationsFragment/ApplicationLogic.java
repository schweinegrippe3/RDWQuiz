package de.ddoering.rdwquiz.fragments.ClassificationsFragment;

import android.os.Bundle;
import android.widget.AdapterView;

/**
 * Container for the Application Logic from ClassificationsFragment
 * @author ddoering
 */
public class ApplicationLogic {
    //Used vars
    private final ClassificationsFragment classificationsFragment;

    /**
     * Constructor. Defines used vars
     * @param classificationsFragment The host fragment
     */
    public ApplicationLogic(ClassificationsFragment classificationsFragment) {
        this.classificationsFragment = classificationsFragment;
    }

    /**
     * Notifys the host activity if a classification has been selected
     * @param position the clicked position in the list. Add 1 to get the classification
     */
    void OnClassificationSelectd(int position) {
        //Prepare notification
        Bundle data = new Bundle();
        data.putInt(ClassificationsFragment.EXTRA_CLASSIFICATION, position + 1);

        //Send notification
        classificationsFragment.getClassificationsInteractionListener().onClassificationsInteraction(
                ClassificationsFragment.ACTION_CLASSIFICATION_SELECTED, data);
    }
}
