package de.ddoering.rdwquiz.fragments.QuestConfirmFragment;

import de.ddoering.rdwquiz.models.Answer;

/**
 * Container for the Data logic inside the QuestConfirmFragment
 * @author dvogeley
 */
public class Data {
    //The correct answer, which belongs to the question
    private final Answer correctAnswer;
    //Instanz of the host QuestConfirmFragment
    private final QuestConfirmFragment questConfirmFragment;

    /**Constructor. Searches for the correct answer and stores it inside the vars
     * @param questConfirmFragment The host fragment
     */
    public Data(QuestConfirmFragment questConfirmFragment) {
        this.questConfirmFragment = questConfirmFragment;
        correctAnswer = Answer.find(Answer.class, "question = ?", String.valueOf(questConfirmFragment.getQuestId())).get(0);
    }

    /**
     * Getter for the correct ANswer
     * @return correctAnswer the correct Answer
     */
    Answer getCorrectAnswer() {
        return correctAnswer;
    }
}
