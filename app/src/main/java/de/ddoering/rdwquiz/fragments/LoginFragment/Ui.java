package de.ddoering.rdwquiz.fragments.LoginFragment;

import android.app.Activity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.User;

/**
 * @author cwagner
 * Diese Klasse stellt das UserInterface für den Login bereit - siehe dazu auch das Layout fragment_login (Zur Verfügung stellen von Texten, Buttons etc.)
 * Der User kann sich mit einem Benutzernamen registrieren, einen bestehenden zur Anmeldung auswählen, sich danach anmelden oder auch die App verlassen.
 */
public class Ui {

    /**
     * Deklaration der benötigten Variablen, v.a. im Bezug auf das erstellte Layout (Buttons, Textfields)
     */
    private final Button btnQuit;
    private final Button btnLogin;
    private final EditText etUsername;
    private final Spinner usernameSpinner;
    private final LoginFragment loginFragment;

    /**
     * Konstruktur der UI für den Login. Initialisiert die zuvor deklarierten Variablen, d.h. ordnet diesen den entsprechenden Bestandteilen aus dem Layout zu
     * @param view: Die zum finden der einzelnen Bestandteile notwendige View wird übergeben
     * @param loginFragment: Das für die UI notwendige Fragment wird übergeben und initalisiert
     */
    public Ui(View view, LoginFragment loginFragment) {
        // Zuordnung der Variablen über die findViewByID-Methode zu den Bestandteilen des Layouts
        btnQuit = (Button) view.findViewById(R.id.f_login_quitButton);
        btnLogin = (Button) view.findViewById(R.id.f_login_loginButton);
        etUsername = (EditText) view.findViewById(R.id.f_login_usernameInput);
        usernameSpinner = (Spinner) view.findViewById(R.id.f_login_user_spinner);
        this.loginFragment = loginFragment;

        //Aktualisieren der UI in jedem Fall z.B. um Liste der Benutzernamen im Spinner zu aktualisieren
        updateUi();
    }

    /**
     * Getter-Methode für den Quit-Button
     * @return: gibt den Beenden Button zurück
     */
    public Button getBtnQuit() {
        return btnQuit;
    }

    /**
     * Getter-Methode für den Login-Button
     * @return: Gibt den Anmelden Button zurück
     */
    public Button getBtnLogin() {
        return btnLogin;
    }

    /**
     * Textfeld zur Eingabe des Benutzernamens
     * @return Zeigt den Benutzernamen an
     */
    public EditText getEtUsername() {
        return etUsername;
    }

    /**
     * Zeigt die Liste der Benutzernamen im/als Spinner an
     * @return Gibt den Spinner zurück
     */
    public Spinner getUsernameSpinner() {
        return usernameSpinner;
    }

    /**
     * Dient dem Aktualisieren es UserInterfaces falls Änderungen aufgetreten sind (z.B. neuer Benutzer für Spinner erstellt). Wird jedes Mal aufgerufen.
     */
    void updateUi() {
        etUsername.setText("");

        //Daten für den usernameSpinner aus Liste lesen und setzen
        List<User> users = User.listAll(User.class);
        users.add(0, new User("Benutzer auswaehlen"));
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(loginFragment.getActivity().getApplicationContext(), R.layout.spinner_default_black, users);
        usernameSpinner.setAdapter(adapter);
    }
}
