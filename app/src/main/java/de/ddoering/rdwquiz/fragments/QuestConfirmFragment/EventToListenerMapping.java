package de.ddoering.rdwquiz.fragments.QuestConfirmFragment;

import android.view.View;

import de.ddoering.rdwquiz.R;

/**
 * Map events inside the QUestConfirmFragment
 * @author dvogeley
 */
public class EventToListenerMapping implements View.OnClickListener {
    private final ApplicationLogic applicationLogic;
    private final Ui ui;
    private final QuestConfirmFragment questConfirmFragment;

    /**
     * Constructor. Defines used vars and sets mapping
     * @param applicationLogic ApplicationLogic to use for the mapping
     * @param ui Ui to use for mapping
     * @param questConfirmFragment Instance of the host fragment
     */
    public EventToListenerMapping(ApplicationLogic applicationLogic, Ui ui, QuestConfirmFragment questConfirmFragment) {
        this.applicationLogic = applicationLogic;
        this.ui = ui;
        this.questConfirmFragment = questConfirmFragment;

        //set mapping
        ui.getButtonCorrect().setOnClickListener(this);
        ui.getButtonWrong().setOnClickListener(this);
    }

    /**
     * Gets fired if a button gets clicked. Fires the applications logic after seitching over the viewId
     * @param v
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.f_confirm_button_yes:
                applicationLogic.btnCorrectClicked();
                break;
            case R.id.f_confirm_button_no:
                applicationLogic.btnWrongClicked();
                break;
        }
    }
}
