package de.ddoering.rdwquiz.fragments.QuestConfirmFragment;

/**
 * Container for the application logic inside the QuestConfirmFragment
 * @author dvogeley
 */
public class ApplicationLogic {
    //Instance of the host fragment
    private final QuestConfirmFragment questConfirmFragment;

    /**
     * Constructor. Sets the used vars
     * @param questConfirmFragment Instance of the host fragment
     */
    public ApplicationLogic(QuestConfirmFragment questConfirmFragment) {
        this.questConfirmFragment = questConfirmFragment;
    }

    /**
     * Gets fired if the correct button gets clicked. Informs the host activity of the fragment
     */
    void btnCorrectClicked() {
        //Inform activity
        questConfirmFragment.getQuestConfirmationAction().onQuestConfirmInteraction(QuestConfirmFragment.ACTION_ANSWER_CORRECT, null);
    }

    /**
     * Gets fired if the wrong button gets clicked. Informs the host activity of the fragment
     */
    void btnWrongClicked() {
        //Inform activity
        questConfirmFragment.getQuestConfirmationAction().onQuestConfirmInteraction(QuestConfirmFragment.ACTION_ANSWER_WRONG, null);
    }
}
