package de.ddoering.rdwquiz.fragments.QuestShowFragment;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.models.Answer;
import de.ddoering.rdwquiz.models.Question;

/**
 * @author peter
 */

/**
 * Bildung von neuen Instancen mit Abhängigkeiten, Aufruf der Fragen
 * Generierung der Klasse QuestShowFragmen welche von Fragment erbt
 * Deklaration der Variablen
 */
public class QuestShowFragment extends Fragment {
    private static final String ARG_QUESTION_ID = "questionId";
    private static final String ARG_READ_ONLY = "ro";
    public static final String ACTION_SUBMITTED = "questSubmitted";
    public static final String ACTION_QUEST_SEEN = "questSeen";
    public static final String EXTRA_ANSWER_TEXT = "answerText";
    public static final String EXTRA_ANSWER_IDS = "answerIds";

    private long questionId;
    private boolean readOnly;

    private ApplicationLogic applicationLogic;
    private Data data;
    private EventToListenerMapping eventToListenerMapping;
    private Ui ui;

    private OnQuestShowInteractionListener questShowInteractionListener;

    /**
     * Bildung einer neuen Instance anhand questionId
     *
     * @param questionId: übergabe questionId
     * @return: neue Instance
     */
    public static QuestShowFragment newInstance(long questionId) {
        return newInstance(questionId, false);
    }

    /**
     * Bildung einer neuen Instance anhand questionId und boolean Bedingung
     *
     * @param questionId: übergabe questionId
     * @param readOnly: boolean
     * @return: fragment
     */
    public static QuestShowFragment newInstance(long questionId, boolean readOnly) {
        QuestShowFragment fragment = new QuestShowFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_QUESTION_ID, questionId);
        args.putBoolean(ARG_READ_ONLY, readOnly);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * leerer Konstruktor
     */
    public QuestShowFragment() {
        // Required empty public constructor
    }

    /**
     *
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            questionId = getArguments().getLong(ARG_QUESTION_ID);
            readOnly = getArguments().getBoolean(ARG_READ_ONLY);
        }
    }

    /**
     * Erzeugen und Wiedergabe der Frage
     *
     * @param inflater: einbindung in die Activity
     * @param container: generierung der parameter für view
     * @param savedInstanceState: sichert Status
     * @return: view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Frage");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quest_show, container, false);

        data = new Data(this);
        applicationLogic = new ApplicationLogic(this, data);
        ui = new Ui(this, data, view);
        eventToListenerMapping = new EventToListenerMapping(this, applicationLogic, ui);

        return view;
    }

    /**
     * Aufruf des Fragment, nachdem es mit der Activity verbunden wurde
     * @param activity: paramenter der activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            questShowInteractionListener = (OnQuestShowInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnQuestShowInteractionListener");
        }
    }

    /**
     * Aufruf des Fragments, sobald es nicht mehr mit der Activity verbunden ist
     */
    @Override
    public void onDetach() {
        super.onDetach();
        questShowInteractionListener = null;
    }

    /**
     * Verbindung zu OnQuestShowInteractionListener
     */
    public interface OnQuestShowInteractionListener {
        public void onQuestShowInteraction(String what, Bundle data);
    }

    /**
     * Rückgabe
     * @return: questionId
     */
    public long getQuestionId() {
        return questionId;
    }

    /**
     * Rückgabe
     * @return: readOnly
     */
    public boolean isReadOnly() {
        return readOnly;
    }

    /**
     * Rückgabe
     * @return: questShowInteractionListener
     */
    public OnQuestShowInteractionListener getQuestShowInteractionListener() {
        return questShowInteractionListener;
    }
}
