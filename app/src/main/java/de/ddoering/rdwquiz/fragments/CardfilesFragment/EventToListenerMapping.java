package de.ddoering.rdwquiz.fragments.CardfilesFragment;

import android.view.View;
import android.widget.AdapterView;

/**
 * This class is appropriate for the events that can choose in the cardfile.
 * Here are events initialised and further actions initiate.
 *
 * This class is called in the CardfilesFragment.
 *
 * @author czschoer
 */
public class EventToListenerMapping implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    //private ApplicationLogic
    private final ApplicationLogic applicationLogic;
    //private Ui
    private final Ui ui;

    /**
     * Creates a instance of this class with delivered data.
     *
     * @param applicationLogic transfer the ApplicationLogic for the CardfilesFragments
     * @param ui transfer the die GUI of the CardfilesFragments
     */
    public EventToListenerMapping(ApplicationLogic applicationLogic, Ui ui) {

        this.applicationLogic = applicationLogic;
        this.ui = ui;

        //puts events
        ui.getListView().setOnItemClickListener(this);
        ui.getListView().setOnItemLongClickListener(this);
    }

    /**
     * This method calls a method if a cardfile was clicked.
     * Is executed when an object in the AdapterView is clicked
     *
     * @param parent transfer the AdapterView where the event was threw
     * @param view transfer a view which was clicked
     * @param position transfer a position where was clicked
     * @param id transfer the ID of the row that was clicked
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        applicationLogic.CardfileClicked(parent, position);
    }

    /**
     * This method calls a method if the cardfile is long clicked.
     * Is executed when an object in the AdapterView was clicked long.
     *
     * @param parent transfer the AdapterView where the event was threw
     * @param view transfer a view which was clicked long
     * @param position transfer a position where was clicked
     * @param id transfer the ID of the row which was long clicked
     * @return gives the boolean true back
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        applicationLogic.CardfileLongCLicked(parent, position);
        return true;
    }
}
