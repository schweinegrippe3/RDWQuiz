package de.ddoering.rdwquiz.fragments.StatsFragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import de.ddoering.rdwquiz.R;
import de.ddoering.rdwquiz.data.visualizer.StatsAdapter;
import de.ddoering.rdwquiz.models.Cardfile;
import de.ddoering.rdwquiz.models.Stat;

/**
 * @auther peter
 */

/**
 * Aufruf der Statistiken und speichern des Status
 * Klasse StatsFragment welche von Fragment erbt
 * Deklaration der Variablen
 */
public class StatsFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_CARDFILE_ID = "cardfileId";
    private static final String ARG_USER_ID = "userId";

    private long cardfileId;
    private long userId;

    private Data data;
    private Ui ui;

    /**
     * Erzeugen einer neuen Instance
     *
     * @param classificationId: Angabe der Classification anhand der ID
     * @param userId: Angabe der User anhand der ID
     * @return: Rückgabe fragment
     */
    public static StatsFragment newInstance(long classificationId, long userId) {
        StatsFragment fragment = new StatsFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_CARDFILE_ID, classificationId);
        args.putLong(ARG_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * leerer Konstruktor
     */
    public StatsFragment() {
        // Required empty public constructor
    }

    /**
     * Erzeugen einer neuen Activity
     * @param savedInstanceState: sichert den Status
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cardfileId = getArguments().getLong(ARG_CARDFILE_ID);
            userId = getArguments().getLong(ARG_USER_ID);
        }
    }

    /**
     * Erzeugen und Wiedergabe der Statistiken
     * @param inflater: Einbindung in die Activity
     * @param container: Generierung des Layoutparameter für den View
     * @param savedInstanceState: sichert den Status
     * @return: view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Statistiken");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stats, container, false);

        data = new Data(this, cardfileId, userId);
        ui = new Ui(this, view, data);

        return view;
    }

    /**
     * Rückgabe
     * @return: cardfileId
     */
    public long getCardfileId() {
        return cardfileId;
    }
}
