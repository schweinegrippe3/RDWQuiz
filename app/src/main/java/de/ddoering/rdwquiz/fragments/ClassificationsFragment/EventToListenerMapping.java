package de.ddoering.rdwquiz.fragments.ClassificationsFragment;

import android.view.View;
import android.widget.AdapterView;

/**
 * This class is used to map events inside the fragment on Logic inside the ApplicationLogic
 * @author ddoering
 */
public class EventToListenerMapping implements AdapterView.OnItemClickListener {
    private final ApplicationLogic applicationLogic;

    /**
     * Constructor. Going to define the mapping
     * @param applicationLogic ApplicationsLogic which should be used for mapping
     * @param ui  Ui which should be used for mapping
     */
    public EventToListenerMapping(ApplicationLogic applicationLogic ,Ui ui) {
        this.applicationLogic = applicationLogic;

        //map events
        ui.getListView().setOnItemClickListener(this);
    }

    /**
     * Going to be fired onCLick inside the classification list
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        applicationLogic.OnClassificationSelectd(position);
    }
}
