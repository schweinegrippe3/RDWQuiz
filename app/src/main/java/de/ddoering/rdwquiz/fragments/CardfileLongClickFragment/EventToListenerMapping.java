package de.ddoering.rdwquiz.fragments.CardfileLongClickFragment;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

/**
 * Events that are used for a long click on a cardfile are defined here.
 *
 * This class is called in the CardfileLongClickFragment.
 *
 * @author czschoer
 */
public class EventToListenerMapping implements AdapterView.OnItemClickListener {
    // private CardfileLongClickFragment
    private final CardfileLongClickFragment cardfileLongClickFragment;
    //private UI
    private final Ui ui;
    //private ApplicationLogic
    private final ApplicationLogic applicationLogic;

    /**
     * Creates a instance of this class with the transferred data.
     *
     * @param cardfileLongClickFragment transfer a instance of the cardfileLongClickFragments
     * @param ui transfer the GUI of the cardfileLongClickFragments
     * @param applicationLogic transfer the applicationLogic ot the cardfileLongClickFragments
     */
    public EventToListenerMapping(CardfileLongClickFragment cardfileLongClickFragment, Ui ui, ApplicationLogic applicationLogic) {
        this.cardfileLongClickFragment = cardfileLongClickFragment;
        this.ui = ui;
        this.applicationLogic = applicationLogic;

        ui.getListView().setOnItemClickListener(this);
    }

    /**
     * This method is called when a cardfile is long clicked.
     * Depending on the position the statistic is shown or the quiz is started.
     *
     * @param parent transfer the AdapterView where the event was thrown
     * @param view transfer the view that was clicked long
     * @param position transfer the position where was long clicked
     * @param id transfer the ID of the row which was long clicked
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0 || position == 1)
            applicationLogic.onQuestStartClicked(position);
        else if(position == 2)
            applicationLogic.onStatsClicked();
    }
}
