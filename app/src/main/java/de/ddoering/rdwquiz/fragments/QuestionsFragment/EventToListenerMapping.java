package de.ddoering.rdwquiz.fragments.QuestionsFragment;

import android.view.View;
import android.widget.AdapterView;

/**
 * EventToListenerMapping connects an action (tht the user does) with the right method.
 * First of all the "OnItemClickListener" gets implemented.
 * Initialisation for the variables: applicationLogic, data
 * @author smartens
 */

public class EventToListenerMapping implements AdapterView.OnItemClickListener {
    private final ApplicationLogic applicationLogic;
    private final Data data;

    /** EventToListenerMapping()
     * Hand out the variables: ui, applicationLogic, data
     * Constructor, Initialisation of the variables: applicationLogic, data
     * @param questionsFragment
     * @param ui
     * @param applicationLogic
     */

    public EventToListenerMapping(Ui ui, ApplicationLogic applicationLogic, Data data) {
        this.applicationLogic = applicationLogic;
        // ListView (out of Ui.java) should set the adapter on the instance of this class.
        this.data = data;
        ui.getListView().setOnItemClickListener(this);
    }

    /** onItemClick()
     * If there is more than one question the method gets overridden.
     * Now the logic out of ApplicationLogic.java can be implemented.
     * @param parent
     * @param view
     * @param position
     * @param id
     */

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(data.getQuestions().size() > 0)
            applicationLogic.OnQuestionSelected(parent, position);
    }
}
