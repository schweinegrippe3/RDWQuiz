package de.ddoering.rdwquiz.fragments.LoginFragment;

import android.os.Bundle;
import android.text.Editable;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import de.ddoering.rdwquiz.MainActivity;
import de.ddoering.rdwquiz.models.User;

/**
 * Die hinter dem für das LoginFragment steckende Logik für Aktionen, die ein EventListener erkannt hat
 * @author cwagner
 */
public class ApplicationLogic {

    // entsprechendes Fragment und dazugehöriges UI wird benötigt
    private final LoginFragment loginFragment;
    private final Ui ui;

    /**
     * Konstruktor für die ApplicationLogic
     * @param loginFragment betreffendes Fragment zu dem diese Logik gehören soll
     * @param ui betreffendes UI zu dem diese Logik gehören soll
     */
    public ApplicationLogic(LoginFragment loginFragment, Ui ui) {
        this.loginFragment = loginFragment;
        this.ui = ui;
    }

    /**
     * Methode reagiert auf Klick des Benutzers auf "Anmelden"
     * @param etUsername Username mit dem sich angemeldet wurde, wird übergeben
     */
    void btnLoginClicked(EditText etUsername) {
        //Prüft, ob der Benutzername bereits vergeben wurde (Kann ein Benutzer gefunden werden (Name ist größer als 0), ist einer vorhanden
        int userCount = User.find(User.class, "name = ?", etUsername.getText().toString()).size();
        if(userCount > 0)
        {
            //Benutzer gefunden, Fehlermeldung ausgeben
            Toast.makeText(loginFragment.getActivity().getApplicationContext(), "Username bereits vergeben", Toast.LENGTH_SHORT).show();
            return;
        }
        Bundle data = new Bundle();
        //Übergabe des Namens an die Activity
        data.putString(MainActivity.EXTRA_USERNAME, etUsername.getText().toString());
        //Benzutzer noch nicht vorhanden, Login durchführen
        loginFragment.getInteractionListener().onLoginInteraction(loginFragment.ACTION_LOGIN, data);
    }

    /**
     * Methode reagiert auf Klick des Nutzers auf "Beenden"
     */
    void btnQuitClicked() {
        loginFragment.getInteractionListener().onLoginInteraction(loginFragment.ACTION_QUIT, null);
    }

    /**
     * Prüft Eingabe des Benutzers im Hinblick auf den Benutzernamen
     * Dieser muss größer 0 sein, darf also nicht leer sein, ist dies der Fall, wird der "Anmelden"-Button zur Verfügung gestellt
     * @param s Übergabe des gewählten Benutzernamens
     */
    public void onEtUsernameChanged(Editable s) {
        if (s.toString().length() > 0)
            ui.getBtnLogin().setEnabled(true);
        else
            ui.getBtnLogin().setEnabled(false);
    }

    /**
     * Wird Benutzer aus Spinner gewählt, gibt diese Methode diesen an die Activity weiter
     * @param parent Spinner
     * @param position Gibt die Position des gewählten Namens im Spinner an
     */
    public void userSelected(AdapterView<?> parent, int position) {
        if (position == 0)
            return;

        // User auswählen
        User user = (User) parent.getAdapter().getItem(position);
        Bundle data = new Bundle();
        //User an Activity weitergeben und Login durchführen
        data.putString(MainActivity.EXTRA_USERNAME, user.getName());
        loginFragment.getInteractionListener().onLoginInteraction(loginFragment.ACTION_LOGIN, data);
        //Login durchgeführt, Spinner wieder  zurücksetzen
        ui.getUsernameSpinner().setSelection(0);
    }
}
