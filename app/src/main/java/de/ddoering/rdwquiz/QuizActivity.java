package de.ddoering.rdwquiz;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.ddoering.rdwquiz.data.DbHandlerThread;
import de.ddoering.rdwquiz.fragments.CardfileLongClickFragment.CardfileLongClickFragment;
import de.ddoering.rdwquiz.fragments.CardfilesFragment.CardfilesFragment;
import de.ddoering.rdwquiz.fragments.ClassificationsFragment.ClassificationsFragment;
import de.ddoering.rdwquiz.fragments.QuestionsFragment.QuestionsFragment;
import de.ddoering.rdwquiz.fragments.StatsFragment.StatsFragment;
import de.ddoering.rdwquiz.models.Question;
import de.ddoering.rdwquiz.models.User;
import de.ddoering.rdwquiz.models.UserQuestion;

/**
 * Hostet das eigentliche Quiz. Erwartet als Extra einen gültigen Usernamen in MainActivity.EXTRA_USERNAME
 * @author czschoer, mmießeler, smartens
 */
public class QuizActivity extends ActionBarActivity
        implements CardfilesFragment.OnCardfilesInteractionListener, ClassificationsFragment.OnClassificationsInteractionListener
        ,QuestionsFragment.OnQuestionsInteractionListener, FragmentInteraction{
    private static final String TAG = "QuizActivityDebugTag";
    private static final String STATE_USERNAME = "username";
    private static final String STATE_USER_ID = "userId";
    private static final String STATE_CARDFILE_ID = "cardfileId";
    private static final String STATE_CLASSIFICATION_ID = "classificationId";
    private static final String STATE_UI_STATE = "uiState";
    private static final String STATE_QUEST_IDS = "questIds";
    private static final String TAG_FRAGMENT_CONTEXT = "contextFragment";
    private static final String TAG_FRAGMENT_QUESTIONS = "questionsFragment";
    private static final String TAG_FRAGMENT_CARDFILES = "cardfilesFragment";
    private static final int REQ_SINGLE_QUEST = 1;
    private static final int REQ_MULTI_QUEST = 2;

    private static final int UI_STATE_CARDFILE = 1;
    private static final int UI_STATE_CLASSIFICATION = 2;
    private static final int UI_STATE_QUESTION = 3;
    private static final int UI_STATE_CONTEXT = 4;
    private static final int UI_STATE_STATS = 5;

    //private String that holds the username
    private String username;
    //private number that represents the ID of the current User
    private long userId;
    //private number that represents the ID of the current Cardfile
    private long cardfileId;
    //private number that represents the ID of the current Classification
    private int classificationId;
    //private number that represents the active Frameweork. It expects one of the static UI-STATE numbers
    private int uiState;
    //An Arraylist of numbers, each representing the ID of one question
    private ArrayList<Long> questIds;

    /**
     * This Method is executed when Launching the Activity. It will initialize all needed Data.
     *
     * @param savedInstanceState This Parameter holds an existing frozen State of the Activity. If there is no frozen State the Parameter is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        Bundle bundle = getIntent().getExtras();
        username = bundle.getString(MainActivity.EXTRA_USERNAME);
        Log.d(TAG, "Username is " + username);

        init(savedInstanceState);
    }

    /**
     * Shows the Menu where the User can Log Off.
     *
     * @param menu Holds the Menu that will be added
     * @return True to make the menu visble, false to make it invisible
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    /**
     * This method is executed when an item in the menu is selected.
     * It will then perform the Action that is represented by the selected Item.
     *
     * @param item The Item, that has been selected by the User.
     * @return boolean Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //The ID of the item
        int id = item.getItemId();

        // if the users pressed the logoff menu, then the activity will finish.
        if (id == R.id.a_quiz_action_logout) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Holds the actual state of the Activity before the activity is getting killed.
     * This secures, that you can reload the state when returning to the activity.
     *
     * @param outState In this bundle the State of the activity is getting saved.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /*
            The username, userID, cardfileID, classificationID, uiState and all questions
            are put into the bundle.
         */
        outState.putString(STATE_USERNAME, username);
        outState.putLong(STATE_USER_ID, userId);
        outState.putLong(STATE_CARDFILE_ID, cardfileId);
        outState.putInt(STATE_CLASSIFICATION_ID, classificationId);
        outState.putInt(STATE_UI_STATE, uiState);

        if(questIds != null) {
            long[] tmpQuestIds = new long[questIds.size()];
            for(int i = 0; i<questIds.size(); i++)
                tmpQuestIds[i] = questIds.get(i);
            outState.putLongArray(STATE_QUEST_IDS, tmpQuestIds);
        }
    }

    /**
     * This Method initiates all Data that is needed for the Quiz.
     *
     * @param savedInstanceState This Parameter holds an existing frozen State of the Activity. If there is no frozen State the Parameter is null.
     */
    private void init(Bundle savedInstanceState) {
        /*
            If there is no frozen InstanceState then the Instance State is getting loaded.
            If the active uiState is the Cardfile then all Cardfiles are getting upgraded.
            Finally the Fragment is getting opened and the User is getting checked.
         */
        if(savedInstanceState == null)
        {
            uiState = UI_STATE_CARDFILE;

            DbHandlerThread updateThread = new DbHandlerThread(this, new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    Bundle data = msg.getData();
                    if(data != null)
                    {
                        String action = data.getString(DbHandlerThread.EXTRA_ACTION);
                        if(action.equals(DbHandlerThread.ACTION_THREAD_COMPLETE)){
                            if(uiState == UI_STATE_CARDFILE)
                            {
                                CardfilesFragment fragment = (CardfilesFragment)getFragmentManager().findFragmentByTag(TAG_FRAGMENT_CARDFILES);
                                fragment.update();
                                Toast.makeText(getApplicationContext(),"Karteikarten aktuallisiert", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
            updateThread.start();

            FragmentTransaction tn = getFragmentManager().beginTransaction();
            tn.add(R.id.a_quiz_fragment_container, CardfilesFragment.newInstance(username), TAG_FRAGMENT_CARDFILES);
            tn.addToBackStack(null);
            tn.commit();

            ensureUser(username);
        }
        /*
            If there is an existing frozen State then the Data given by the Attributes of the class are getting initialized.
         */
        else {
            username =savedInstanceState.getString(STATE_USERNAME);
            userId = savedInstanceState.getLong(STATE_USER_ID);
            cardfileId = savedInstanceState.getLong(STATE_CARDFILE_ID);
            classificationId = savedInstanceState.getInt(STATE_CLASSIFICATION_ID);
            uiState = savedInstanceState.getInt(STATE_UI_STATE);
            questIds = new ArrayList<Long>();
            if(savedInstanceState.getLongArray(STATE_QUEST_IDS) != null)
                for(long id : savedInstanceState.getLongArray(STATE_QUEST_IDS))
                    questIds.add(id);
        }
    }

    /**
     * Checks whether the user is already existing in the Database and if it is not existing it is getting added to the database
     * @param username The username which is getting checked
     * @return true if the user is existing, false if the user is not existing
     */
    private boolean ensureUser(String username){
        if(User.count(User.class, "name = ?", new String[]{username}) <= 0)
        {
            User user = new User(username);
            User.saveInTx(user);
            userId = user.getId();

            return false;
        } else
            userId = User.find(User.class,"name = ?", username).get(0).getId();
            return true;
    }

    /**
     * This method is executed when a category/cardfile is clicked.
     * Here is a differentiation made, if the click is long or not. Then there will be other methods
     * called and a instance of the ClassficationFragments or the CardfileLongClickFragments is
     * created. The fragment will be displayed.
     *
     * @param what transfer the action that was made, here if a long or short click
     * @param data transfer a bundle with information about the cardfile-ID
     */
    @Override
    public void onCardfilesInteraction(String what, Bundle data) {
        //a cardfile was clicked. Load a new fragment
        if(what.equals(CardfilesFragment.ACTION_CARDFILE_CLICKED))
        {
            cardfileId = data.getLong(CardfilesFragment.EXTRA_CARDFILE_ID);
            FragmentTransaction tn = getFragmentManager().beginTransaction();
            ClassificationsFragment fragment = ClassificationsFragment.newInstance(data.getLong(CardfilesFragment.EXTRA_CARDFILE_ID), userId);
            tn.replace(R.id.a_quiz_fragment_container, fragment);
            tn.addToBackStack(null);
            tn.commit();
            uiState = UI_STATE_CLASSIFICATION;
            setTitle("Verfügbare Klassen");
        }
        //a cardfile was long clicked. Load a new fragment
        else if(what.equals(CardfilesFragment.ACTION_CARDFILE_LONG_CLICKED))
        {
            long id = data.getLong(CardfilesFragment.EXTRA_CARDFILE_ID);
            FragmentTransaction tn = getFragmentManager().beginTransaction();
            CardfileLongClickFragment fragment = CardfileLongClickFragment.newInstance(id);
            tn.replace(R.id.f_cardfiles_overlay_container, fragment, TAG_FRAGMENT_CONTEXT);
            tn.commit();
            uiState = UI_STATE_CONTEXT;
        }

    }

    /**
     * This method is executed when a classification was clicked.
     * Create a new instance of the QuestionsFragment and displays the new fragment.
     * Methods will be called.
     *
     * @param what transfer the action that was executed
     * @param data transfer a bundle with information about the classification-ID
     */
    @Override
    public void onClassificationsInteraction(String what, Bundle data) {

        //a classification was clicked. Load new fragment
        if(what.equals(ClassificationsFragment.ACTION_CLASSIFICATION_SELECTED))
        {
            classificationId = data.getInt(ClassificationsFragment.EXTRA_CLASSIFICATION);
            FragmentTransaction tn = getFragmentManager().beginTransaction();
            QuestionsFragment fragment = QuestionsFragment.newInstance(userId, cardfileId, classificationId);
            tn.replace(R.id.a_quiz_fragment_container, fragment, TAG_FRAGMENT_QUESTIONS);
            tn.addToBackStack(null);
            tn.commit();
            setTitle("Verfügbare Fragen");
            uiState = UI_STATE_QUESTION;
        }
    }

    /** onQuestionInteraction()
     * Definition of an intent.
     * @param what
     * @param data
     */
    @Override
    public void onQuestionInteraction(String what, Bundle data) {
        if(what.equals(QuestionsFragment.ACTION_QUESTION_CLICKED))
        {
            // The content of the Bundle gets saved in the long variable tmpQuestionId
            long tmpQuestionId = data.getLong(QuestionsFragment.EXTRA_QUESTION_ID);
            // Creating a new intent
            Intent intent = new Intent(getApplicationContext(), QuestActivity.class);
            // The information questionId is put/saved in the intent.
            intent.putExtra(QuestActivity.EXTRA_QUESTION_ID, tmpQuestionId);
            // The information userId is put/saved in intent.
            intent.putExtra(QuestActivity.EXTRA_USER_ID, userId);
            // Hand over the intent and the variable REQ_SINGLE_QUEST
            startActivityForResult(intent, REQ_SINGLE_QUEST);

        }
    }

    /** onActivityResult()
     * User has the chance to decide, whether he wants to answer a single question ore multiple questions.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
     protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //With the super-class the method inherits every functionality of the fathermethod
        super.onActivityResult(requestCode, resultCode, data);
        // If the user just wants to answer one single question the if statement starts.
        if(requestCode == REQ_SINGLE_QUEST)
        {
            //Update die Fragen. Eine Frage kann die Klasse gewechselt haben
            ((QuestionsFragment)getFragmentManager().findFragmentByTag(TAG_FRAGMENT_QUESTIONS)).updateQuestions();
        }
        // If the user wants to answer more than one question in a request "if else" starts.
        else if(requestCode == REQ_MULTI_QUEST)
        {
            //Soll die naechste Frage angezeigt werden?
            if(resultCode != QuestActivity.RESULT_ABORTED)
                startQuizzing();
            // If the test gets interrupted, ui gets initialised with UI_STATE_CARDFILE
            else
                uiState = UI_STATE_CARDFILE;
        }
    }

    /**
     * This method will be executed on pressing back.
     * The Method will check the Current State of the Activity and will do action depending on the current State.
     * If the user presses back then the Method changes to the fragment that has to be shown
     * @author mmießeler
     */
    @Override
    public void onBackPressed() {
        /*
        If the Cardfile is open, the actitivity will end
        If stats or one list Classifications are open, the List Of Cardfiles will be shown.
        If a question is open, the list of Classifcations is activated.
        If Context is open, the fragment closes and the UI returns to the list of Cardfiles.
         */
        switch (uiState){
            case UI_STATE_CARDFILE:
                finish();
                break;
            case UI_STATE_STATS:
            case UI_STATE_CLASSIFICATION:
                FragmentTransaction tn = getFragmentManager().beginTransaction();
                tn.replace(R.id.a_quiz_fragment_container, CardfilesFragment.newInstance(username));
                tn.commit();
                uiState = UI_STATE_CARDFILE;
                break;
            case UI_STATE_QUESTION:
                Bundle data = new Bundle();
                data.putLong(CardfilesFragment.EXTRA_CARDFILE_ID, cardfileId);
                onCardfilesInteraction(CardfilesFragment.ACTION_CARDFILE_CLICKED, data);
                uiState = UI_STATE_CLASSIFICATION;
                break;
            case UI_STATE_CONTEXT:
                getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentByTag(TAG_FRAGMENT_CONTEXT)).commit();
                uiState = UI_STATE_CARDFILE;
                break;
        }
    }

    /**
     * This method returns a list of all questions of the given cardfile that have to been answered.
     *
     * @param cardfileId The ID of the current Cardfile
     * @param orderBy Represents how the List should be ordered
     * @return A List of all questions that are ready to be answered
     */
    private List<Question> getOverdueQuestions(long cardfileId, String orderBy) {
        String[] args = new String[18];

        /*
         Select will only show those questions which last answer was longer ago than the defined time
         Select only returns Data from current Cardfile and current user.
         Select, that holds all Information about the User and the Questions
         */
        String query = "SELECT * FROM (SELECT\n" +
                "  q.*\n" +
                "FROM QUESTION q,\n" +
                "     USER_QUESTION uq\n" +
                "WHERE uq.question = q.id\nAND (";

        int index = 0; int classification = 1;
        while (classification <= 7) {
            query += "(uq.classification = ? AND uq.last_answer <= ?)" + (classification != 7 ? " or\n" : ")\n");
            args[index++] = String.valueOf(classification);
            long millisToRemove = Configs.TIME_CLASSIFICATIONS[classification-1] * 1000L;
            args[index++] = String.valueOf(System.currentTimeMillis() - millisToRemove);
            classification++;
        }
        query += "and q.cardfile = ? and uq.user = ?\n" +
                "UNION\n" +
                "Select * from " + Question.getTableName(Question.class) + " where id not in (" +
                "Select question from " + UserQuestion.getTableName(UserQuestion.class) + " where user = ?) and " +
                "cardfile = ?) src ORDER BY " + orderBy;
        args[14] = String.valueOf(cardfileId);
        args[15] = String.valueOf(userId);
        args[16] = String.valueOf(userId);
        args[17] = String.valueOf(cardfileId);
        List<Question> questions = Question.findWithQuery(Question.class, query, args);

        // If there is no question that has to been answered then a Message is given
        if(questions.size() <= 0)
            Toast.makeText(getApplicationContext(), "Keine fälligen Fragen gefunden", Toast.LENGTH_SHORT).show();

        return questions;
    }

    /**
     * This method is executed when a long click on a category was made and then a click on one of
     * the menu buttons.
     * There is a request made if the click was on the starting of the quiz or the statistics.
     * If the user wishes to start the quiz, there are functions used to select the order and the cardfile.
     * The bundle gives information about the order the user wishes. Then the quiz is started.
     * If the user wishes to get his statistics, there are functions to show this.
     * Therefor a new instance of the StatsFragment must be created and is displayed.
     *
     * @param what transfer the actions that was made
     * @param data transfer a bundle with information about the cardfile-ID and the order
     */
    @Override
    public void onFragmentInteraction(String what, Bundle data) {
        if(what.equals(CardfileLongClickFragment.ACTION_CONTEXT_START_QUIZZ))
        {
            long cardfileId = data.getLong(CardfileLongClickFragment.EXTRA_CARDFILE_ID);
            String orderBy = data.getString(CardfileLongClickFragment.EXTRA_ORDER_BY);
            List<Question> questions = getOverdueQuestions(cardfileId, orderBy);

            getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentByTag(TAG_FRAGMENT_CONTEXT)).commit();

            ArrayList<Long> tmpQuestIds = new ArrayList<>(questions.size());
            for(Question q : questions)
                tmpQuestIds.add(q.getId());

            questIds = tmpQuestIds;
            startQuizzing();
        } else if(what.equals(CardfileLongClickFragment.ACTION_CONTEXT_SHOW_STATS)) {
            long cardfileId = data.getLong(CardfileLongClickFragment.EXTRA_CARDFILE_ID);

            StatsFragment fragment = StatsFragment.newInstance(cardfileId, userId);

            getFragmentManager().beginTransaction().replace(R.id.a_quiz_fragment_container, fragment).commit();
            uiState = UI_STATE_STATS;
        }
    }

    /** startQuizzing()
     * Function that starts, if the user wants a test with more than one question.
     */
    public void startQuizzing() {
        //If the questId is smaller than one (there are no questions at the moment), the if statement starts.
        if(questIds.size() < 1)
        //the User wont see any question
            return;
        // the first question is at the first position, called 0 and is saved in an long variable
        long tmpQuestionId = questIds.get(0);
        //the first position gets reset, because the user is answering the question
        questIds.remove(0);
        //Creating a new intent
        Intent intent = new Intent(getApplicationContext(), QuestActivity.class);
        // The information questionId is put/saved in the intent.
        intent.putExtra(QuestActivity.EXTRA_QUESTION_ID, tmpQuestionId);
        // The information userId is put/saved in intent.
        intent.putExtra(QuestActivity.EXTRA_USER_ID, userId);
        // The Activity for the results starts.
        startActivityForResult(intent, REQ_MULTI_QUEST);

    }
}
